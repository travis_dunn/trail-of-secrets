namespace :npm do
  task :symlink, :roles => :app do
    shared_dir = File.join(shared_path, 'node_modules')
    release_dir = File.join(current_path, 'node_modules')
    run("mkdir -p #{shared_dir} && ln -nfs #{shared_dir} #{release_dir}")
  end

  task :install, :roles => :app do
    run "cd #{release_path} && sudo npm install"
  end

  task :update, :roles => :app do
    run "cd #{release_path} && npm install --link"
  end  

  desc "List installed npm modules"
  task :list, :roles => :app do
    run "cd #{current_path} && npm list"
  end
end

# NB: Uncomment the following two lines to use install-per-deploy strategy
# after 'deploy:cold', 'npm:install'
# after 'deploy:finalize_update', 'npm:update'