set :application, "trail_of_secrets"
set :domain, 'trailofsecrets.com'

set :user, "ubuntu"
set :admin_runner, user
set :use_sudo, false

set :node_path,   "/usr/bin"
set :node_script, "app.js"