// gamestate object handling when game is stale, etc
var Game = function() {
  this.gameData = null
  this.cookieName = 'tos.game'  

}

Game.prototype.hasPlayer = function hasPlayer() {
  return $.cookie(this.cookieName) ? true : false
}

Game.prototype.load = function load(err, data) {
  // set cookie for 24 hours
  $.cookie(gameCookieName, true, { expires: 1})
}

exports.Game = Game