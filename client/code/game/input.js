var PlayerInput = function() {
  var self = this

  this.idle = function() {
    self.input = 'idle'
    $.tos.hud.setTitle()
  }
  this.isIdle = function() { return self.input === 'idle' }
  this.active = function() {
    self.input = 'active'
  }
  this.isActive = function() { return self.input === 'active' }
  this.power = function() {
    self.input = 'power'
    $.tos.hud.setTitle()
  }  
  this.isPower = function() { return self.input === 'power' }
  this.wait = function() {
    self.input = 'waiting'
  }
  this.isWaiting = function() { return self.input === 'waiting' }
  this.resolving = function() {
    self.input = 'resolving'
  }
  this.isResolving = function() { return self.input === 'resolving' }

  self.input = 'waiting' // start in disabled, waiting state
}

exports.Player = PlayerInput