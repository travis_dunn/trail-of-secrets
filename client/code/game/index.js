// Prevent elastic page scrolling on touchscreens
$(document).bind('touchmove', function(e) {
  e.preventDefault();
});

// prevent firefox from dragging UI elements (e.g. tiles)
$(document).bind("dragstart", function() {
   return false;
});

require('/gameboard');
require('/hud/index');
require('/insanity');
require('/powers');

require('/tos');

require('/mythos');
require('/supplies');
require('/advancement');
require('/turn');