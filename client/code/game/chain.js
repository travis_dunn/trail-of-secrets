MIN_MATCH_COUNT = 3

function createLine(l1, t1, l2, t2) {
  x1 = Math.min(l1, l2)
  x2 = Math.max(l1, l2)
  y1 = Math.min(t1, t2)
  y2 = Math.max(t1, t2)  

  var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))
  var angle  = Math.atan2(t2 - t1, l2 - l1) * 180 / Math.PI;
  
  var transform = angle === 0 || angle === 180 ? '' : 'rotate('+angle+'deg)';

  var line = $('<div>')
    .addClass('chain')
    .css({
      '-moz-transform': transform,
      '-o-transform': transform,
      '-webkit-transform': transform,
      '-ms-transform': transform,
      'transform': transform,
      'left': x1,
      'top': y1
    })
    .width(length)

  return line
}

var Chain = function(gameboard) {
  this.gameboard = gameboard
  this.cells = []
  this.lines = []
}
Chain.prototype._disconnect = function _disconnect() {
  this.lines.pop().remove()
}
Chain.prototype._connect = function _connect() {
  if (this.cells.length < 2) return

  var prev = $(this.previousCell().cell)
  var cur = $(this.currentCell().cell)

  var container= $('#game')
  var basePosition = container.position()
  var fullWidth = prev.outerWidth()
  var halfWidth = fullWidth / 2
  var quarterWidth = fullWidth / 4

  var topOffset = this.previousCell().y === this.currentCell().y ? halfWidth : fullWidth
  var leftOffset = this.previousCell().x === this.currentCell().x ? 0 : halfWidth
  var obliqueOffset = this.previousCell().x !== this.currentCell().x && 
                      this.previousCell().y !== this.currentCell().y ? quarterWidth : 0

  var l1 = prev.position().left + leftOffset - obliqueOffset
  var t1 = prev.position().top + topOffset
  var l2 = cur.position().left + leftOffset - obliqueOffset
  var t2 = cur.position().top + topOffset

  var line = createLine(l1, t1, l2, t2)

  container.prepend(line)
  this.lines.push(line)
}
Chain.prototype._isMatchLine = function _isMatchLine() {
  var firstCell = this.gameboard.cellFromIndex(this.cells[0])
  for (var i = 1; i < this.cells.length; i++) { 
    var cell = this.gameboard.cellFromIndex(this.cells[i])
    if (cell.x !== firstCell.x && cell.y !== firstCell.y) return false
  }
  return true
}
Chain.prototype._hasSelectedShambler = function _hasSelectedShambler() {
  var board = this.gameboard
  return this.cells.filter(function(i){ 
    var monster = board.cellFromIndex(i).tile.model.monster
    return monster && monster._id === 'dimensional_shambler' 
  }).length > 0
}
Chain.prototype._hasNonTchoTcho = function _hasNonTchoTcho(monsters) {
  return monsters.filter(function(c) { return !c.hasMonster('tcho_tcho') }).length > 0
}
Chain.prototype._hasGnophKeh = function _hasGnophKeh(monsters) {
  return monsters.filter(function(c) { return c.hasMonster('gnoph_keh') }).length > 0
}
Chain.prototype._hasLengSpiders = function _hasLengSpiders(monsters) {
  return monsters.filter(function(c) { return c.hasMonster('spider_of_leng') }).length > 0
}
Chain.prototype._hasSelectedBelow = function _hasSelectedBelow(cell) {
  var board = this.gameboard
  return this.cells.filter(function(i) { 
    var c = board.cellFromIndex(i)
    return c.index !== cell.index && c.y >= cell.y 
  }).length > 0
}
Chain.prototype._hasSelectedAbove = function _hasSelectedAbove(cell) {
  var board = this.gameboard
  return this.cells.filter(function(i) { 
    var c = board.cellFromIndex(i)
    return c.index !== cell.index && c.y < cell.y 
  }).length > 0
}
Chain.prototype._monsterCount = function _monsterCount(id) {
  var monsters = this.gameboard.monsterCells()
  var board = this.gameboard
  return monsters.filter(function(c) { return c.hasMonster(id) }).length
}
Chain.prototype._hasChainLimit = function _hasChainLimit() {
  var board = this.gameboard
  var cnt = this._monsterCount('elder_thing')
  return cnt > 0 && this.cells.length < MIN_MATCH_COUNT + cnt 
}
Chain.prototype._canInvestigate = function _canInvestigate() {
  return this.currentCell().tile.model.tileType !== 'investigation' ||
         this._monsterCount('shoggoth') === 0
}
Chain.prototype._canRecharge = function _canRecharge() {
  return this.currentCell().tile.model.tileType !== 'magic' ||
         this._monsterCount('wizard') === 0
}
Chain.prototype._canRecoverAndFund = function _canRecoverAndFund() {
  var cellType = this.currentCell().tile.model.tileType
  return (cellType !== 'recovery' && cellType !== 'resource') ||
         this._monsterCount('colour_out_of_space') === 0
}
Chain.prototype._muteCell = function _muteCell(cell) {
  cell.tile.tile.addClass('muted')
}
Chain.prototype._unmuteCell = function _unmuteCell(cell) {
  cell.tile.tile.removeClass('muted')
}
Chain.prototype._updateCellCollectable = function _updateCellCollectable(index, conditions) {
  var cell = this.gameboard.cellFromIndex(index)
  if (conditions.hasChainLimit) {
    this._muteCell(cell)
  } else if (!conditions.canInvestigate) {
    this._muteCell(cell)
  } else if (!conditions.canRecoverAndFund) {
    this._muteCell(cell)
  } else if (!conditions.canRecharge) {
    this._muteCell(cell)
  } else if (cell.tile.monster) {
    if (conditions.hasShambler && !cell.hasMonster('dimensional_shambler')) {
      this._muteCell(cell)
    } else if (cell.hasMonster('ghost') && $.tos.hud.turnCount % 2 === 1) {
      this._muteCell(cell)
    } else if (cell.hasMonster('hound_of_tindalos') && !this._isMatchLine()) {
      this._muteCell(cell)
    } else if (cell.hasMonster('nightgaunt') && this._hasSelectedBelow(cell)) {  
      this._muteCell(cell)
    } else if (cell.hasMonster('shantak') && this._hasSelectedAbove(cell)) {  
      this._muteCell(cell)
    } else if (cell.hasMonster('lloigor') && $.tos.hud.model.magic.current < 1) {  
      this._muteCell(cell)
    } else if (cell.hasMonster('dark_young') && cell.isEvenRow()) {  
      this._muteCell(cell)
    } else if (cell.hasMonster('tcho_tcho') && this._hasNonTchoTcho()) {  
      this._muteCell(cell)
    } else {
      this._unmuteCell(cell)
    }
  } else if (conditions.hasLengSpiders) {
    var spiders = this.gameboard.monsterCells().filter(function(c) { return c.monster._id === 'spider_of_leng' })
    for (var i = 0; i < spiders.length; i++) {
      if (spiders[i].y === cell.y) {
        this._muteCell(cell)
        break
      }
    }
  } else if (conditions.hasGnophKeh) {
    var gnophKeh = this.gameboard.monsterCells().filter(function(c) { return c.monster._id === 'gnoph_keh' })
    for (var i = 0; i < gnophKeh.length; i++) {
      if (this.gameboard.isIndexAdjacent(gnophKeh[i].index, index)) {
        this._muteCell(cell)
        break
      }
    }
  } else {
    this._unmuteCell(cell)
  }
}
Chain.prototype._updateCellCollectability = function _updateCellCollectability() {
  var cells = this.gameboard.monsterCells()
  var self = this

  // store lookups for conditions which prevent collection
  var conditions = {
    hasShambler: this._hasSelectedShambler(cells),
    hasGnophKeh: this._hasGnophKeh(cells),
    hasLengSpiders: this._hasLengSpiders(cells),
    hasChainLimit: this._hasChainLimit(),
    canInvestigate: this._canInvestigate(),
    canRecharge: this._canRecharge(),
    canRecoverAndFund: this._canRecoverAndFund()
  }

  // update tiles which are not collectable (because of monster ability, etc.)
  for (var i = 0; i < this.cells.length; i++) {
    this._updateCellCollectable(this.cells[i], conditions)
  }
}
Chain.prototype.currentIndex = function currentIndex() {
  return this.cells.length > 0 ? this.cells[this.cells.length-1] : null
}
Chain.prototype.previousCell = function previousCell() {
  if (this.cells.length < 2) return null

  var index = this.cells[this.cells.length-2]
  return this.gameboard.cellFromIndex(index)
}
Chain.prototype.currentCell = function currentCell() {
  var index = this.currentIndex()
  return index !== null ? this.gameboard.cellFromIndex(index) : null
}
Chain.prototype.hasIndex = function hasIndex(index) {
  return this.cells.some(function(i) { 
    return i === index;
  })
}
Chain.prototype.isMatchComplete = function isMatchComplete() {
  return this.cells.length >= MIN_MATCH_COUNT
}
Chain.prototype.isEmpty = function isEmpty() {
  return this.cells.length === 0
}
Chain.prototype.isPreviousCell = function isPreviousCell(cell) {
  return cell.isSelected() &&
         this.cells.length > 1 &&
         this.cells[this.cells.length-2] === cell.index
}
Chain.prototype.isNewCell = function isNewCell(cell) {
  return this.currentIndex() !== cell.index
}
Chain.prototype.isWaitingOnMonsterInfo = function isWaitingOnMonsterInfo() {
  return this.monsterInfoTimeout ? true : false
}
Chain.prototype.cancelMonsterInfo = function cancelMonsterInfo() {
  if (this.monsterInfoTimeout) {
    clearTimeout(this.monsterInfoTimeout)
    this.monsterInfoTimeout = null
  }
  return this
}
Chain.prototype.getMonsterInfo = function getMonsterInfo(monsterId) {
  this.cancelMonsterInfo() // cancel any pending info request

  var delay = $.tos.monsters[monsterId] ? 2000 : 1200

  var self = this
  self.monsterInfoTimeout = setTimeout( function() {
    $.tos.monster(monsterId, function(monster) {
      self.monsterInfoTimeout = null
      $.tos.dialogs.monster(monster)
      $.tos.input.idle()
      $.tos.gameboard.deselect()
    })
  }, delay)
}
Chain.prototype.select = function select(cell) {
  if (this.isPreviousCell(cell)) {
    this.cancelMonsterInfo()
    var index = this.pop() // backtracking to previous cell so remove the top cell from the chain
    this.gameboard.cellFromIndex(index).deselect()
    this._updateCellCollectability()
    this._disconnect()
  } else {
    if (cell.isSelected()) return false // if cell already in selected state return immediately

    if (this.isEmpty()) {
      // if first cell in chain then ignore matching logic and update board ui
      this.gameboard.deactivateTiles(cell.tile) 

      // if monster cell then start pending info request
      if (cell.tile.monster) this.getMonsterInfo( cell.tile.monster.model._id)
    } else if (!this.isNewCell(cell)) {
      return false // if already selecting top cell from chain
    } else if (!this.gameboard.isIndexAdjacent(this.currentIndex(), cell.index)) {
      return false // if cells aren't adjacent 
    } else if (!this.gameboard.tilesMatch(this.currentCell().tile, cell.tile)) {
      return false // is tile types don't match
    } else {
      this.cancelMonsterInfo()
    }

    this.add(cell)
  }
  return true
}
Chain.prototype.add = function add(cell) {
  var monsterCells = this.gameboard.monsterCells()
  cell.select(monsterCells) // update cell ui
  this.push(cell.index)
  this._updateCellCollectability()
  this._connect()
}
Chain.prototype.push = function push(index) {
  if (this.hasIndex(index)) {
    return 
  } else {
    this.cells.push(index)
  }
}
Chain.prototype.pop = function pop() {
  return this.cells.pop()
}
Chain.prototype.clear = function clear() {
  for (var i = 0; i < this.lines.length; i++) {
    this.lines[i].remove()
  }
  return this
}

exports.Chain = Chain