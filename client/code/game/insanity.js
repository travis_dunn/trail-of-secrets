$.fn.insanity = function(insanities) {
  var self = this

  this.addInsanity = function(insanity) {
    // update insanity if it already exists
    for (var i = 0; i < self.insanities.length; i++) {
      if (self.insanities[i]._id === insanity._id) {
        self.insanities[i].name = insanity.name
        self.insanities[i].level = insanity.level
        var label = self.find('#'+insanity._id)
        label.html(insanity.name)
        return
      }
    }

    if (this.insanities.length === 0) self.empty()

    // the insanity is new so add it to the list
    self.insanities.push(insanity)
    var row = $("<tr class='error'><td><strong id='"+insanity._id+"'>"+insanity.name+"</strong></td><td>"+insanity.description+"</td></tr>")
    self.append(row)
  }

  this.insanities = []
  for (var i = 0; i < (insanities || []).length; i++) {
  	self.addInsanity(insanities[i])
  }
  if (this.insanities.length === 0) {
    self.append("<tr class='error'><td>You are of <strong>sound mind</strong> for now, but madness comes quick!<br /><br /> When your sanity reaches zero you will gain a random debilitating insanity that will affect you until death.</td></tr>")
  }

  return self
}