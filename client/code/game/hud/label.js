var HUDLabel = function(name, value) {
  this.name = name

  this.current = $('#'+name)
  this.total = $('#total-'+name)

  this._load(value)
}
HUDLabel.prototype._load = function _load(data) {
  this.data = $.isNumeric(data) ? {current: parseInt(data), total: parseInt(data)} : data
  this._updateLabel()
}
HUDLabel.prototype._updateLabel = function _updateLabel() {
  this.current.html(this.data.current)
  if (this.total.length) this.total.html(this.data.total)
}
HUDLabel.prototype.modify = function modify(delta) {
  delta = parseInt(delta)
  if (delta > 0) this.flash(delta)
  var newCurrent = this.data.current + delta
  this.data.current = Math.max(newCurrent, 0)
  this._updateLabel()
}
HUDLabel.prototype.bounce = function bounce() {
  var el = $('#'+this.name).parent()
  el.addClass('animated bounce')
  setTimeout( function() { el.removeClass('animated bounce') }, 1000);
}
HUDLabel.prototype.flash = function flash(value) {
  var sign = value < 0 ? '' : '+'
  var el = $("<span class='flash-message animated longest fade-out-down'>"+sign+value+" <span>"+this.name+"</span></span>")
  if (value < 0) el.css('color', 'red')
  this.current.parent().append(el)

  var interval = value < 0 ? 2000 : 1500
  setTimeout( function() {
    el.remove()
  }, interval);
}
HUDLabel.prototype.score = function score(fromElement, icon) {
  var parent = this.current.parent()

  var fromOffset = fromElement.offset()
  var parentOffset = parent.offset()
  var x = parseInt(fromOffset.left - parentOffset.left)
  var y = parseInt(fromOffset.top - parentOffset.top)

  icon = icon || this.name
  var score = $("<span class='animated shorter shrink-to score "+icon+"'></span>")
  score.css('left', x+'px')
  score.css('top', y+'px')
  parent.append(score)
}

exports.Label = HUDLabel