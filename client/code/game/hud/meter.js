var HUDMeter = function(name, data) {
  this.name = name
  this.current = $('#'+name)
  this.total = $('#total-'+name)
  this.bar = $('#'+name+'-bar')
  this.label = this.bar.parent().children('label')

  this._load(data)
}
HUDMeter.prototype._load = function _load(data) {
  this.data = data
  this._updateBar()
  this._updateLabels()
}
HUDMeter.prototype._updateBar = function _updateBar() {
  var percentage = parseInt((this.data.current / this.data.total) * 100)
  this.bar.css('width', percentage+'%')
}
HUDMeter.prototype._updateLabels = function _updateLabels() {
  this.current.html(this.data.current)
  this.total.html(this.data.total)
}
HUDMeter.prototype.increase = function increase(delta) {
  this.data.total += delta
  this.data.current += delta
  this._updateBar()
  this._updateLabels()
}
HUDMeter.prototype.modify = function modify(delta) {
  if (delta > 0) this.flash(delta)
  var newCurrent = this.data.current + delta
  this.data.current = Math.min(Math.max(newCurrent, 0), this.data.total)
  this._updateBar()
  this._updateLabels()
}
HUDMeter.prototype.reset = function reset() {
  this.data.current = this.data.total
  this._updateBar()
  this._updateLabels()
}
HUDMeter.prototype.set = function set(value) {
  this.data.current = value
  this._updateBar()
  this._updateLabels()
}
HUDMeter.prototype.flash = function flash(value) {
  var sign = value < 0 ? '' : '+'
  var el = $("<span class='flash-message animated longest fade-out-down'>"+sign+value+" <span>"+this.name+"</span></span>")
  if (value < 0) el.css('color', 'red')
  this.label.append(el)

  var interval = value < 0 ? 2000 : 1500
  setTimeout( function() {
    el.remove()
  }, interval);
}
HUDMeter.prototype.score = function score(fromElement, icon) {
  var parent = this.bar.parents('.scores')

  var fromOffset = fromElement.offset()
  var parentOffset = parent.offset()
  var x = parseInt(fromOffset.left - parentOffset.left)
  var y = parseInt(fromOffset.top - parentOffset.top)

  icon = icon || this.name
  var score = $("<span class='animated shrink-to score "+icon+"'></span>")
  score.css('left', x+'px')
  score.css('top', y+'px')
  parent.prepend(score)
}

exports.Meter = HUDMeter