var HUDLabel = require('./label').Label
var HUDMeter = require('./meter').Meter

$.fn.hud = function(json, seals, turn) {
  var self = this
  this.model = json
  this.model.seals = seals

  var drawer = $('#drawers')

  this.health = new HUDMeter('health', this.model.health)
  this.sanity = new HUDMeter('sanity', this.model.sanity)
  this.wards = new HUDMeter('wards', this.model.wards)
  this.magic = new HUDMeter('magic', this.model.magic)

  this.exp = new HUDMeter('exp', 
    {current: this.model.experience.current, total: this.model.experience.next}  // normalize exp json for meter
  )

  this.turn = self.find('#turn')
  this.turn.html(turn)
  self.turnCount = turn

  this.seals = new HUDLabel('seals', seals)
  this.wealth = new HUDLabel('wealth', this.model.wealth)

  this.level = self.find('#level')
  this.level.html(this.model.level)


  this.skillModifier = function(skill) {
    var equipment = self.model.equipment
    if (!equipment) return 0

    function modFor(equipment) {
      var mod = 0
      for (var i = 0; i < equipment.modifiers.length; i++) {
        var item = equipment.modifiers[i]
        if (item._id === skill) {
          mod += item.value
        }
      }
      return mod
    }

    var mod = 0
    if (equipment.accessory) mod += modFor(equipment.accessory)
    if (equipment.attire) mod += modFor(equipment.attire)
    if (equipment.weapon) mod += modFor(equipment.weapon)
    return mod
  }

  this.setSkill = function(name, value) {
    var label = $('#'+name)
    var mod = self.skillModifier(name)

    label.parent().find('.badge-info').remove()
    if (mod > 0) {
      label.parent().append(" <span class='badge badge-info'>+"+mod+"</span>")
    }

    self.model.skills[name] = value
    drawer.find('#'+name).html(value)
  }

  this.setSkills = function() {
    self.setSkill('adventure', self.model.skills.adventure)
    self.setSkill('conflict', self.model.skills.conflict)
    self.setSkill('occult', self.model.skills.occult)
    self.setSkill('research', self.model.skills.research)
    self.setSkill('hunting', self.model.skills.hunting)
    self.setSkill('healing', self.model.skills.healing)
    self.setSkill('funding', self.model.skills.funding)
    self.setSkill('augury', self.model.skills.augury)
    self.setSkill('arcana', self.model.skills.arcana)
  }

  this.setEquipment = function(name, item) {
    if (item) this.model.equipment[name] = item
    item = this.model.equipment[name]
    var label = $('#'+name)
    var description = $('#'+name+'-description')

    label.html(item.name)
    description.html(item.description)
  }

  if (this.model.equipment) {
    if (this.model.equipment.accessory) this.setEquipment('accessory')
    if (this.model.equipment.attire) this.setEquipment('attire')
    if (this.model.equipment.weapon) this.setEquipment('weapon')
  }
  this.setSkills()

  this.addAdventure = function(delta) {
    self.model.skills.adventure += delta
    this.setSkill('adventure', this.model.skills.adventure)
  }
  this.addConflict = function(delta) {
    self.model.skills.conflict += delta
    this.setSkill('conflict', this.model.skills.conflict)
  }
  this.addOccult = function(delta) {
    self.model.skills.occult += delta
    this.setSkill('occult', this.model.skills.occult)
  }
  this.addResearch = function(delta) {
    self.model.skills.research += delta
    this.setSkill('research', this.model.skills.research)
  }
  this.addHunting = function(delta) {
    self.model.skills.hunting += delta
    this.setSkill('hunting', this.model.skills.hunting)
  }
  this.addHealing = function(delta) {
    self.model.skills.healing += delta
    this.setSkill('healing', this.model.skills.healing)
  }
  this.addFunding = function(delta) {
    self.model.skills.funding += delta
    this.setSkill('funding', this.model.skills.funding)
  }
  this.addAugury = function(delta) {
    self.model.skills.augury += delta
    this.setSkill('augury', this.model.skills.augury)
  }
  this.addArcana = function(delta) {
    self.model.skills.arcana += delta
    this.setSkill('arcana', this.model.skills.arcana)
  }
  this.addLevels = function(delta) {
    self.model.level += delta
    self.level.html(self.model.level)
  }

  this.incrementTurn = function(delta) {
    delta = delta || 1
    self.turnCount = self.turnCount + delta
    self.turn.text(self.turnCount)
  }

  this.setTitle = function() {
  if ($.tos.input.isIdle() || $.tos.input.isResolving()) {
    $('#current-tile').hide()
    $('#header .title').show()
  } else if ($.tos.gameboard.chain && $.tos.gameboard.chain.cells.length > 0) {
    var cell = $.tos.gameboard.chain.currentCell()

    var text = cell.tile.model.tileId
    if (cell.tile.monster) {
      text = cell.tile.monster.model.name
    }
    $('#header .title').hide()
    $('#current-tile').text(text).show()
  } else if ($.tos.input.isPower()) {
    var text = $('#powers a.selected big').text()
    $('#header .title').hide()
    $('#current-tile').text(text).show()
  }
}
  return self
}

require('./audio');