var popup;

exports.Menu = function Menu(action) {
  if (action === 'hide') {
    popup.modal('hide')  
    return
  }

  // initialize popup element once; unlike other popups, don't remove the character profile from the DOM when hidden
  if (!popup) {
    popup = $('#drawers-body')
    popup.addClass('modal fade hide').attr('role', 'dialog').attr('aria-hidden', true).removeClass('hidden-phone')
    var footer = "<div class='modal-footer'><button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button></div>"
    popup.append(footer)

    if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  }

  popup.modal('show')
}

// initialize character profile button
$('#show-profile').click(function(e) {
  e.preventDefault()
  $.tos.dialogs.character()
  return false
})