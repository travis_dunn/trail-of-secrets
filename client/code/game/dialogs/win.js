function scoreTable(score) {
  var html = "<table class='table table-condensed'>"
  html += "<tr><td>Stat</td><td>Score</td></tr>"
  html += "<tr class='success'><td><strong>SCORE</strong></td><td><strong>"+score.score+"</strong></td></tr>"
  html += "<tr><td>MATCHES</td><td>"+score.matches+"</td></tr>"
  html += "<tr><td>TURNS</td><td>"+score.turns+"</td></tr>"
  html += "<tr><td>WEALTH</td><td>"+score.wealth+"</td></tr>"
  html += "<tr><td>EXP</td><td>"+score.experience+"</td></tr>"
  html += "<tr><td>KILLS</td><td>"+score.kills+"</td></tr>"
  html += "</table>"
  return html
}

exports.Menu = function Menu(scores, unlockedGOO, cb) {
  var popup = $("<div id='death' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'></div>")    
  var header = $("<div class='modal-header'><h3>You Prevailed</h3></div>")  
  var body = $("<div class='modal-body'><p>Against all odds, you sealed the great evil.</p>"+scoreTable(scores[0])+"</div>")
  if (unlockedGOO) body.append("<p>You discovered "+unlockedGOO+" and may now challenge them.</p>")

  var footer = $("<div class='modal-footer'><button class='btn' data-dismiss='modal' aria-hidden='true'>Continue</button></div>")

  // cleanup and invoke callback (if applicable)
  popup.on('hidden', function(){
    popup.remove()
    if (typeof cb === 'function') cb()
  })

  // prompt anonymous users to login
  $.tos.dialogs._addLoginControls(body, footer)

  popup.append(header).append(body).append(footer).appendTo('body')

  if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  popup.modal('show')
}