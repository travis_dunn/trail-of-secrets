CURRENCY_MAP = {
  'USD': '$',
  'CAD': '$',
  'AUD': '$',
  'EUR': '€',
  'WON': '₩',
  'GBP': '£',
  'JPY': '¥',
  'INR': '₹'
}

var popup;

function cleanup() {
  popup.remove()
  popup = null
}

function purchase(item) {
  return function(e) {
    e.preventDefault()

    $.facebook.buy(item._id, function(result) {
      if (result.order_id) {
        switch (item._id) {
          case 'unexpected_inheritance':
            $.user.patronage += 10
            break
          case 'mysterious_benefactor':
            $.user.patronage += 50
            break
          case 'masonic_fellowship':
            $.user.patronage += 100
            break
          case 'wilmarth_foundation_endowment':
            $.user.patronage += 1000
            break
        }
      } else if (result.error_code) {
      }
      popup.modal('hide')
    })
    return false
  }
}

function itemRow(item) {
  var row = $("<tr><td><strong>"+item.name+"</strong><br /><em>"+item.description+"</em></td></tr>")
  var cashButton = $("<a id='"+item._id+"' class='btn btn-mini btn-primary nowrap'>Buy for <span class='price'>"+item.cost+"</span></a>")

  cashButton.click(purchase(item))

  var btnRow = $("<td style='width:90px'></td>").append(cashButton)
  row.append(btnRow)
  return row
}

function localizeCurrency(cb) {
  $.facebook.currency(function(currency) {
    var table = popup.find('#shop-table')
    var curSymbol = CURRENCY_MAP[currency.user_currency] || currency.user_currency
    var rate = currency.currency_exchange_inverse
    var offset = currency.currency_offset
    var prices = table.find('.price')

    for (var i = 0; i < prices.length; i++) {
      var el = $(prices[i])
      var price = parseInt(el.html())
      var localPrice = String(Math.round(price * rate)); 
      el.html(curSymbol+localPrice)
    }

    cb()
  })
}

function showShopItems(items, cb) {
  var table = popup.find('#shop-table')
  for (var i = 0; i < items.length; i++) {
    table.append( itemRow(items[i]) )
  }    

  localizeCurrency(cb)
}

exports.Menu = function Menu() {
  popup = $("<div id='supplies' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true'>")
  var header = $("<div class='modal-header'><h3>Premium Cash Shop</h3></div>")
  var footer = $("<div class='modal-footer'><button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button></div>")
  var body = $("<div class='modal-body unpadded'></div>")
  var table = $("<table id='shop-table' class='table table-striped' style='margin-bottom='5'></table>")  

  body.append(table)
  popup.append(header).append(body).append(footer).appendTo('body')

  $.tos.premiums(showShopItems, function() {
    if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
    popup.modal('show')
  })

  popup.on('hidden', cleanup)
}