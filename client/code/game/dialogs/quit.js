var popup;

function cleanup() {
  popup.remove()
  popup = null
}

function restart(e) {
  e.preventDefault()
  $.tos.restartGame()     
  return false
}

exports.Menu = function Menu() {
  popup = $("<div id='maddened' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true'>")
  var header = $("<div class='modal-header'><h3>Restart Game?</h3></div>")
  var body = $("<div class='modal-body'>Quitting now will sacrifice all your progress and restart you in a new game. Sometimes, suicide is the only sane option that remains...</big></div>")
  var footer = $("<div class='modal-footer'><button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button></div>")
  var button = $("<button id='restart' class='btn btn-danger' aria-hidden='true'>Quit Game</button>")

  button.click(restart)

  popup.on('hidden', cleanup)  

  footer.append(button)
  popup.append(header).append(body).append(footer).appendTo('body')
  popup.modal('show')  
}

// initialize any ui restart/quit buttons
$('#quit').click(function(e) {
  e.preventDefault()
  $.tos.dialogs.quit()
  return false
})