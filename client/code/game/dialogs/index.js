exports._addLoginControls = function(body, footer) {
  if ($.user.isAuthorized()) return

  var login = $("<button class='btn btn-success'>Login</button>")
  body.append("<p><big>Don't be forgotten.</big> Before continuing take a moment to login - you'll get 10 free patronage and can save your high scores.</p>")
  login.click( $.facebook.login )
  footer.append(login)
}

exports.greatOldOnes = require('./great_old_ones').Menu
exports.win = require('./win').Menu
exports.lose = require('./lose').Menu
exports.stats = require('./stats').Menu
exports.monster = require('./monster').Menu
exports.madness = require('./madness').Menu
exports.quit = require('./quit').Menu
exports.supply = require('./supply').Menu
exports.shop = require('./shop').Menu
exports.level = require('./level_up').Menu
exports.power = require('./power_up').Menu
exports.tutorial = require('./tutorial').Menu
exports.character = require('./character').Menu