function scoreTable(score) {
  var html = "<table class='table table-condensed'>"
  html += "<tr><td>Stat</td><td>Score</td></tr>"
  html += "<tr class='success'><td><strong>SCORE</strong></td><td><strong>"+score.score+"</strong></td></tr>"
  html += "<tr><td>MATCHES</td><td>"+score.matches+"</td></tr>"
  html += "<tr><td>TURNS</td><td>"+score.turns+"</td></tr>"
  html += "<tr><td>WEALTH</td><td>"+score.wealth+"</td></tr>"
  html += "<tr><td>EXP</td><td>"+score.experience+"</td></tr>"
  html += "<tr><td>KILLS</td><td>"+score.kills+"</td></tr>"
  html += "</table>"
  return html
}

exports.Menu = function Menu(scores) {
  var popup = $("<div id='death' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'></div>")
  var header = $("<div class='modal-header'><h3>You Lost</h3></div>")
  var body = $("<div class='modal-body'>"+scoreTable(scores[0])+"</div>")
  var footer = $("<div class='modal-footer'><a href='/game' class='btn' aria-hidden='true'>New Game</a></div>")

  // prompt anonymous users to login
  $.tos.dialogs._addLoginControls(body, footer)

  popup.append(header).append(body).append(footer).appendTo('body')

  if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  popup.modal('show')
}