var popup;

function cleanup() {
  popup.remove()
  popup = null
}

exports.Menu = function Menu(monster) {
  popup = $("<div id='monster-info' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true'>")
  var header = $("<div class='modal-header'><h3>"+monster.name+"</h3></div>")
  var footer = $("<div class='modal-footer'><button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button></div>")
  var body = $("<div class='modal-body unpadded'></div>")
  var table = $("<table class='table table-striped' style='margin-bottom:0'></table>")  
  table.append("<tr><td style='width:1%;text-align:center;' rowspan='4'><a class='monster "+monster._id+"'></a><br /><strong>Exp: "+monster.experience+"</strong></td><td>Health</td><td>"+monster.health+"</td></tr>")
  table.append("<tr><td>Damage</td><td>"+monster.damage+"</td></tr>")
  table.append("<tr><td>Terror</td><td>"+monster.terror+"</td></tr>")
  table.append("<tr><td>Defense</td><td>"+monster.defense+"</td></tr>")
  table.append("<tr class='info'><td colspan='3'>"+monster.description+"</td></tr>")

  body.append(table)
  popup.append(header).append(body).append(footer).appendTo('body')
  
  popup.on('hidden', cleanup)

  if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  popup.modal('show')  
}