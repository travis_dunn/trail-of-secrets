var popup;

function notEnoughPatronage(item) {
  return !$.user || $.user.patronage < item.patronage ? true : false
}

function notEnoughFunds(item) {
  return item.cost > $.tos.hud.model.wealth ? true : false
}

function flashFunds(btn) {
  var label = btn.find('span')
  label.addClass('animated short infinite flash')
  setTimeout( function() { 
    label.removeClass('animated short infinite flash'); 
  }, 1000)    
}

function showPremiumShop(e) {
  e.preventDefault()
  popup.modal('hide') 

  // if user is logged into facebook (i.e. can make purchaes) then show the shop, else prompt for login
  if ($.user.isAuthorized()) {
    setTimeout( $.tos.dialogs.shop, 500)
  } else {
    setTimeout( $.facebook.login, 500)
  }
  return false
}

function patronage() {
  return $.user ? $.user.patronage : 0    
}
function wealth() {
  return $.tos.hud.model.wealth
}
function supplyList() {
  return $.tos.supplies
}

function cleanup() {
  popup.remove()
  popup = null
}

function purchaseByWealth(item) {
  return function(e) {
    e.preventDefault()
  
    if (notEnoughFunds(item)) {
      var btn = $('#'+item._id)
      flashFunds(btn)
      return
    }

    updateItemSold(item)
    buy(item)
  }
}

function purchaseByPatronage(item) {
  return function(e) {
    e.preventDefault()

    if (notEnoughPatronage(item)) {
      showPremiumShop(e)
    } else {
      updateItemSold(item)
      buy(item, 'patronage')
      return false
    }
  }
}

function updateItemSold(item) {
  var btn = popup.find('#'+item._id)

  // update button ui
  btn.unbind('click')
  btn.addClass('disabled').html('Bought')
  popup.find('#premium-'+item._id).remove()

  // update cached item data
  var list = supplyList()
  for (var i = 0; i < list.length; i++) {
    if (list[i]._id === item._id) {
      list[i].sold = true
      break
    }
  }  
}

function itemRow(item) {
  var row = $("<tr><td><strong>"+item.name+"</strong><br /><em>"+item.description+"</em></td></tr>")
  var wealthButton = $("<a id='"+item._id+"' class='btn btn-mini btn-primary nowrap'>Buy for <span><i class='icon-star icon-white'></i>"+item.cost+"</span></a>")
  var patronageButton = $("<a id='premium-"+item._id+"' class='btn btn-mini btn-primary nowrap'>Buy for <span><i class='icon-star-empty icon-white'></i>"+item.patronage+"</span></a>")

  if (item.sold) {
    wealthButton.addClass('disabled').html('Bought')
  } else {
    wealthButton.click( purchaseByWealth(item) )
    patronageButton.click( purchaseByPatronage(item) )
  }

  var btnRow = $("<td style='width:90px'></td>").append(wealthButton)
  if (!item.sold) btnRow.append(patronageButton)

  row.append(btnRow)
  return row
}

function buy(item, paymentType) {
  popup.modal('hide')

  var isUsingPatronage = paymentType === 'patronage'
  $.tos.supply(item._id, isUsingPatronage, function(results) {
    // deduct cost
    if (isUsingPatronage) {
      $.user.modifyPatronage(-item.patronage)
    } else {
      $.tos.hud.model.wealth = Math.max($.tos.hud.model.wealth-item.cost, 0)
      $.tos.hud.wealth.modify(-item.cost)
      $.tos.hud.model.wealth -= item.cost
      $.tos.hud.wealth.flash(-item.cost)
    }

    results.item = item
    $.tos.useItem(results, $.tos.input.idle)
  })  
}

exports.Menu = function Menu() {
  popup = $("<div id='supplies' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true'>")
  var header = $("<div class='modal-header'><h3>Supply Shop <span id='funds' class='label label-success'><i class='icon-star icon-white'></i><span>"+wealth()+"</span></span> <span id='patronage' class='label'><i class='icon-star-empty icon-white'></i><span>"+patronage()+"</span></span></h3><small>Note: supplies randomly restock when you level up.</small></div>")
  var footer = $("<div class='modal-footer'><button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button></div>")
  var body = $("<div class='modal-body unpadded'></div>")
  var table = $("<table class='table table-striped' style='margin-bottom='5'></table>")

  header.find('#patronage').click(showPremiumShop)

  // initialize items
  var list = supplyList()
  for (var i = 0; i < list.length; i++) {
    table.append( itemRow(list[i]) )
  }

  body.append(table)
  popup.append(header).append(body).append(footer).appendTo('body')

  popup.on('hidden', cleanup)

  if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  popup.modal('show')
}

// initialize any ui supply shop buttons
$('#buy-supplies').click(function(e) {
  e.preventDefault()
  $.tos.dialogs.supply()
  return false
})
