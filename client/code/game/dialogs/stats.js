var popup;

function roundNumber(num, place) {
  place = place || 2
  return Math.round(num*Math.pow(10,place))/Math.pow(10,place);
}

function statsTable() {
  // manually construct the html for the stats table
  var tableHtml = "<table id='stats-table' class='table table-striped' style='margin-bottom='5'><tr>"
  tableHtml += "<td><strong>Current score:</strong/><br />"
  tableHtml += "Avg. exp/turn:<br />"
  tableHtml += "Avg. funds/turn:<br />"
  tableHtml += "Avg. kills/turn:<br />"
  tableHtml += "Avg. score/turn:<br />"
  tableHtml += "Plots solved:<br />"
  tableHtml += "Monsters killed:<br />"
  tableHtml += "Power uses:</td>"
  tableHtml += "<td><strong><span id='stats-score'></span></strong><br />"
  tableHtml += "<span id='stats-exp-rate'></span><br />"
  tableHtml += "<span id='stats-funds-rate'></span><br />"
  tableHtml += "<span id='stats-kill-rate'></span><br />"
  tableHtml += "<span id='stats-score-rate'></span><br />"
  tableHtml += "<span id='stats-plots'></span><br />"
  tableHtml += "<span id='stats-monsters'></span><br />"
  tableHtml += "<span id='stats-powers'></span></td>"
  tableHtml += "<td>Clues matched:<br />"
  tableHtml += "Funds matched:<br />"
  tableHtml += "Health matched:<br />"
  tableHtml += "Sanity matched:<br />"
  tableHtml += "Rites matched:<br />"
  tableHtml += "Firearms matched:<br />"
  tableHtml += "Seals matched:<br />"
  tableHtml += "Longest chain:<br /></td>"
  tableHtml += "<td style='width:15%'>"
  tableHtml += "<span id='stats-clues'></span><br />"
  tableHtml += "<span id='stats-funds'></span><br />"
  tableHtml += "<span id='stats-health'></span><br />"
  tableHtml += "<span id='stats-sanity'></span><br />"
  tableHtml += "<span id='stats-magic'></span><br />"
  tableHtml += "<span id='stats-weapons'></span><br />"
  tableHtml += "<span id='stats-seals'></span><br />"
  tableHtml += "<span id='stats-longness'></span></td>"
  tableHtml += "</tr></table>"
  return tableHtml
}

function cleanup() {
  popup.remove()
  popup = null
}

function showStats(results) {
  var goo = results.greatOldOne
  var score = results.highScore


  popup.find('#game-goo').html(goo.name)
  popup.find('#game-difficulty').html('level '+goo.difficulty)
  var table = popup.find('#stats-table')

  table.find('#stats-clues').html(results.clues)
  table.find('#stats-funds').html(results.funds)
  table.find('#stats-health').html(results.health)
  table.find('#stats-magic').html(results.magic)
  table.find('#stats-monsters').html(results.monsters)
  table.find('#stats-plots').html(results.plots)
  table.find('#stats-powers').html(results.powers)
  table.find('#stats-sanity').html(results.sanity)
  table.find('#stats-seals').html(results.seals)
  table.find('#stats-weapons').html(results.weapons)
  table.find('#stats-longness').html(results.longness)

  // safefuard divide by zero
  var moves = results.moves && results.moves > 0 ? results.moves : 1

  table.find('#stats-score').html(score)
  table.find('#stats-exp-rate').html( roundNumber($.tos.hud.model.experience.current / moves) )
  table.find('#stats-funds-rate').html( roundNumber(results.funds / moves) )
  table.find('#stats-kill-rate').html( roundNumber(results.monsters / moves) )
  table.find('#stats-score-rate').html( roundNumber(score / moves) )

  if ($.tos.isSmallScreen()) popup.modalResponsiveFix()
  popup.modal('show')  
}

exports.Menu = function Menu(scores) {
  var self = this

  popup = $("<div id='stats' class='modal fade hide' tabindex='-1' role='dialog' aria-hidden='true'>")
  var header = $("<div class='modal-header'><h3>Game Stats : <span id='game-goo'></span> <small id='game-difficulty'></small></h3></div>")
  var footer = $("<div class='modal-footer'><button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button></div>")
  var body = $("<div class='modal-body unpadded'></div>")

  var table = statsTable()
  body.append(table)
  popup.append(header).append(body).append(footer).appendTo('body')

  popup.on('hidden', cleanup)

  // call server rpc for stats
  $.tos.stats(showStats)
}

// initialize any ui stats buttons
$('#show-stats').click(function(e) {
  e.preventDefault()
  $.tos.dialogs.stats()
  return false
})