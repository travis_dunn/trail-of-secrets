// init period filter
var periods = $('#leaderboard-period')
periods.button()
periods.find('button').click(function(e) {
  e.preventDefault()

  var btn = $(this)
  if (btn.hasClass('active')) return 
  loadGlobalBoard(btn.data('period'))
})

function isViewingPersonalBoard() {
  return $("[data-board-type='personal']").parent('li').hasClass('active')
}
function clearBoard(tableId) {
  $('#'+tableId).find('tbody').empty()
}
function filterBoard(e) {
  e.preventDefault()

  var link = $(this)
  $('#leaderboard-type li.active').removeClass('active')
  link.parent('li').addClass('active')

  var filterType = link.data('type')

  if (isViewingPersonalBoard()) {
    clearBoard('personal-leaderboard')
    loadPersonalScores(filterType)
  } else {
    loadGlobalBoard(null, filterType)
  }
}

// init type filter
var types = $('#leaderboard-type')
types.find('a').click(filterBoard);

var loadPersonalScores = function(type) {
  var opts = {facebook: $.facebook.data}
  ss.rpc('leaderboards.personal', opts, type, function(err, scores){
    if (err === false) {
      $("#personal-leaderboard-register").removeClass('hidden')
      return
    } else if (!scores || scores.length === 0) {
      $("#personal-leaderboard-empty").removeClass('hidden')
      return
    }

    var table = $('#personal-leaderboard')
    var body = table.children('tbody')
    var alreadyLoaded = body.find('td').length > 0
    if (alreadyLoaded) return

    // build table html and append it to the page when finished
    var rowHtml
    for (var i = 0; i < scores.length; i++) {
      var s = scores[i]
      var rankedClass = s.ranked ? " class='success'" : ""
      var row = "<tr"+rankedClass+"><td><em>"+(i+1)+"</em></td><td>"+s.score+"</td><td>"+s.kills+"</td><td>"+s.turns+"</td><td>"+s.experience+"</td><td>"+s.wealth+"</td><td>"+s.created.split('T')[0]+"</td></tr>"
      rowHtml += row
    }
    body.append(rowHtml)
  })      
}

var loadGlobalBoard = function(period, type) {
  var table = $('#global-leaderboard')
  table.empty()

  period = period || periods.find('.active').data('period')
  type = type || types.find('.active a').data('type')

  table.append('<caption>Global Leaderboard: '+types.find('.active a').html()+'</caption>')
  table.append("<tr><td>&nbsp;</td><td>Name</td><td>Level</td><td>Score</td><td>Won</td></tr>")

  var opts = {facebook: $.facebook.data}
  ss.rpc('leaderboards.global', opts, type, period, function(err, doc, awards) {
    if (!doc) return

    var d = new Date(doc.created)
    var caption = d.toLocaleDateString()
    if (period === 'monthly') caption = caption.split(' ')[1]+', '+caption.split(' ')[3]
    table.find('caption').append(' | '+caption);

    var patronage = awards[type === 'score' ? 'primary' : 'secondary'][period]

    for (var i = 0; i < doc.scores.length; i++) {
      var s = doc.scores[i]
      var award = patronage.length > i ? patronage[i]+'&weierp;' : '-'
      var row = "<tr><td>#"+(i+1)+"</td><td>"+s.name+"</td><td>"+1+"</td><td>"+s.score+"</td><td>"+award+"</td></tr>"
      table.append(row)
    }
  })
}

// initialize boards
clearBoard('personal-leaderboard')
loadPersonalScores()
$("a[data-board-type='personal']").click(function(){
  loadPersonalScores()
})
$("a[data-board-type='global']").click(function(){
  if ($('#global-leaderboard').find('td').length > 0) return
  loadGlobalBoard()
})