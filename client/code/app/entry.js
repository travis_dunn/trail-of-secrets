// This file automatically gets called first by SocketStream and must always exist

// Make 'ss' available to all modules and the browser console
window.ss = require('socketstream');

ss.server.on('disconnect', function(){
  console.log('Connection down :-(');
});

ss.server.on('reconnect', function(){
  console.log('Connection back up :-)');
});

function isPlayingGame() {
  return $('#game').length ? true : false
}
function isViewingScores() {
  return $('#leaderboards').length ? true : false
}

ss.server.on('ready', function(){
  // Wait for the DOM to finish loading
  jQuery(function(){
    // conditionally load appropriate components based on presence of DOM containers (i.e. which web page loaded the script)
    if (isPlayingGame()) require('/index');
    if (isViewingScores()) require('/leaderboards');
  });
});
