var CONFIG = require('config');
var mongoose = require('mongoose');
var Leaderboard = mongoose.model('Leaderboard');
var Score = mongoose.model('Score');
var User = mongoose.model('User');

exports.actions = function(req, res, ss) {
  req.use('session')
  req.use('userAuth.authorizeFacebook')

  function findBoard(type, period, cb) {
    var filter = {type: type, period: period}
    Leaderboard.findOne(filter).sort({timestamp: 'desc'}).exec(cb)
  }

  return {
    personal: function(auth, sort) {
      var userId = req.session.userId
      if (userId) {
        sort = sort || 'score'
        var sortFilter = {}
        sortFilter[sort] = 'desc'

        Score.find({userId: userId}).limit(25).sort(sortFilter).exec(function(err, docs) {
          res(err, docs) 
        })
      } else {
        res(false)
      }
    },
    global: function(auth, type, period) {
      var userId = req.session.userId
      if (userId) {
        User.findOne({ _id: userId}, function(err, user) {
          findBoard(type, period, function(err, doc) {
            res(err, doc, CONFIG.Leaderboards.rewards, user)
          })
        })
      } else {
        findBoard(type, period, function(err, doc) {
          res(err, doc, CONFIG.Leaderboards.rewards)
        })
      }
    },
    friends: function(auth, type, friendIds) {
      var userId = req.session.userId
      if (userId) {
        res()
      } else {
        res(false)
      }
    }
  }  
}