var mongoose = require('mongoose');
var Monster = mongoose.model('Monster');

exports.actions = function(req, res, ss) {
  return {
    info: function(monsterId) {
      var lookups = Monster.lookups()
      var monster = new Monster( lookups[monsterId] )
      
      if (monster) {
        var m = monster.serialize()
        m.description = monster.description
        m.experience = monster.experience
        res(null, m)
      } else {
        res('no monster found')
      }
    }
  }
}