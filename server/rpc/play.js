var mongoose = require('mongoose');
var powers = require('./../mechanics/powers')
var turn = require('./../mechanics/turn')
var score = require('./../mechanics/score')

exports.actions = function(req, res, ss) {
  req.use('session')
  req.use('gameFinder.loadActive')

  return {
    match: function(chain) { 
      turn.play(req.game, chain, function(turnResults, scores) {
        req.game.save(function(err) {
          if (scores) {
            res(err, turnResults, scores) 
          } else {
            res(err, turnResults)
          }
        })
      })
    },
    power: function(powerId, tiles) {
      var results = powers.play(req.game, powerId, tiles)

      req.game.save(function(err){
        res(err, results) 
      })
    },
    stats: function() {
      // return game stats and current score
      var stats = JSON.parse(JSON.stringify(req.game.stats))
      stats.greatOldOne = req.game.greatOldOne()
      stats.highScore = score.highScore(req.game)

      res(null, stats) 
    }
  }
}