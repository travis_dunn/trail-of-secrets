var advancement = require('./../mechanics/advancement')

exports.actions = function(req, res, ss) {
  req.use('session')
  req.use('gameFinder.load')

  function respondNoAdvances() {
    res("game does not have advancing investigator")
  }

  return {
    advance: function(advancementIndex) {
      if (req.game.needsAdvancing()) {
        var results = advancement.advance(req.game, advancementIndex)
        req.game.save(function(err){
          res(err, results) 
        })   
      } else {
        respondNoAdvances()
      }
    },
    empower: function(powerId) {
      if (req.game.needsAdvancing()) {
        var results = advancement.empower(req.game, powerId)

        req.game.save(function(err){
          res(err, results) 
        })   
      } else {
        respondNoAdvances()
      }
    }
  }
}