var logger = require('./logger')

function samplingRate(turn) {
  return Math.round(turn / 10) * 10; // round to nearest 10th turn to 'group' turn sampling in blocks of 10
}

exports.sample = function sample(game) {
  var sample = {
    gid: game._id,
    hth: game.investigator.health.current,
    san: game.investigator.sanity.current,
    mad: game.stats.madness,
    exp: game.stats.experience,
    lvl: game.stats.level,
    kls: game.stats.monsters,
    wpn: game.stats.weapons,
    sls: (game.seals.total - game.seals.current),
    trn: samplingRate(game.turn()),
    wth: game.investigator.wealth,
    pwr: game.stats.powers,
    spt: game.stats.spent
  }
  logger.log('analytics', 'Sample', sample)
}