var facebook = require('./../mechanics/facebook')
var mongoose = require('mongoose');
var User = mongoose.model('User');

exports.authorizeFacebook = function() {
  return function(req, res, next) {
    var isAuthorized = req.session.userId
    var auth = req.params[0]

    function respondLogin(user) {
      req.user = user
      req.session.facebookId = user.facebook.userId
      req.session.setUserId(user._id)
      next()
    }

    if (auth && auth.facebook && !isAuthorized) {
      var signedRequest = auth.facebook.signedRequest
      var userPayload = facebook.verifySignature(signedRequest)
      if (userPayload) {
        User.findFacebook(userPayload.user_id, function(err, user) {
          if (user && !user.gameId && req.session.gameId) {
            user.gameId = gameId
            user.save(function(err){
              respondLogin(user)
            })            
          } else if (user) {
            respondLogin(user)
          } else {
            User.register(userPayload.user_id, req.session.gameId, function(err, user) {
              respondLogin(user)
            })
          }
        })
      } else { next() }
    } else { next() }
  }
}