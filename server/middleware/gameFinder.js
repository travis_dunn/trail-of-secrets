var mongoose = require('mongoose');
var Game = mongoose.model('Game');

function respondNext(req, next, game) {
  req.game = game
  return next()
}

exports.load = function() {
  return function(req, res, next) {
    var gameId = req.session.gameId
    if (!gameId) return res("no game with id '"+gameId+"' found")

    Game.findOne({ _id: gameId}, function(err, game) {
      if (err) {
        res(err)
      } else if (!game) {
        res("no game with id '"+gameId+"' found")
      } else {
        return respondNext(req, next, game)
      }
    })
  }
}

exports.loadActive = function(findPlayable) {
  return function(req, res, next) {
    var gameId = req.session.gameId
    if (!gameId) return res("no game with id '"+gameId+"' found")
  
    Game.findPlayable(gameId, function(err, game) {
      if (err) {
        res(err)
      } else {
        return respondNext(req, next, game)
      }
    })
  }
}