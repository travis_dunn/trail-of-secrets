var mongoose = require('mongoose');
var Schema = mongoose.Schema

var cast = function (val) {
  if (!val) return null;
  var P = mongoose.model('Plot');
  plot = new P(val)
  return plot
}

var plots = {
  turn_of_the_screw: {_id: 'turn_of_the_screw', name: 'Turn of the Screw', level: 1},
  lost_hearts: {_id: 'lost_hearts', name: 'Lost Hearts', level: 1},
  a_mysterious_mansion: {_id: 'a_mysterious_mansion', name: 'A Mysterious Mansion', level: 2},
  the_shunned_house: {_id: 'the_shunned_house', name: 'The Shunned House', level: 2},
  two_doctors: {_id: 'two_doctors', name: 'Two Doctors', level: 2},
  the_green_meadow: {_id: 'the_green_meadow', name: 'The Green Meadow', level: 3},
  story_of_the_spectral_ship: {_id: 'story_of_the_spectral_ship', name: 'Story of the Spectral Ship', level: 3},
  the_thing_in_the_moonlight: {_id: 'the_thing_in_the_moonlight', name: 'The Thing in the Moonlight', level: 3},
  the_evil_clergyman: {_id: 'the_evil_clergyman', name: 'The Evil Clergyman', level: 4},
  the_tomb: {_id: 'the_tomb', name: 'The Tomb', level: 4},
  from_beyond: {_id: 'from_beyond', name: 'From Beyond', level: 4},
  out_of_the_aeons: {_id: 'out_of_the_aeons', name: 'Out of the Aeons', level: 5},
  horror_in_the_museum: {_id: 'horror_in_the_museum', name: 'Horror in the Museum', level: 5},
  the_lurking_fear: {_id: 'the_lurking_fear', name: 'The Lurking Fear', level: 6},
  the_unnamable: {_id: 'the_unnamable', name: 'The Unnamable', level: 7},
  the_silver_key: {_id: 'the_silver_key', name: 'The Silver Key', level: 8}
}

var dictionary = {
  clues: { type: Number, default: 0},
  level: Number,
  name: String
}
var Plot = new Schema(dictionary);

Plot.statics.list = function () {
  var a = []
  for (var key in plots) { a.push(plots[key]) }
  return a
}

Plot.methods.isSolved = function () {
  return this.clues && this.clues >= this.level ? true : false
}
Plot.methods.experience = function () {
  return Math.floor(this.level / 2)
}
Plot.methods.serialize = function () {
  var serialObject = {
    _id: this._id, 
    clues: this.clues, 
    name: this.name, 
    level: this.level
  }
  return serialObject
}

module.exports.Plot = Plot
module.exports.Dictionary = dictionary
module.exports.Lookups = plots
module.exports.Cast = cast