var mongoose = require('mongoose');
var Schema = mongoose.Schema

var tiles = {
  artifact: {_id: 'artifact', type: 'resource', chance: 1},
  clue: {_id: 'clue', type: 'investigation', chance: 10},
  dynamite: {_id: 'dynamite', type: 'combat', chance: 1},
  monster: {_id: 'monster', type: 'combat', chance: 10},
  funds: {_id: 'funds', type: 'resource', chance: 17},
  grimoire: {_id: 'grimoire', type: 'magic', chance: 1},
  bandages: {_id: 'bandages', type: 'recovery', chance: 13},
  morphine: {_id: 'morphine', type: 'recovery', chance: 1},
  plot: {_id: 'plot', type: 'investigation', chance: 3},
  rite: {_id: 'rite', type: 'magic', chance: 14},
  tincture: {_id: 'tincture', type: 'recovery', chance: 5},
  laudanum: {_id: 'laudanum', type: 'recovery', chance: 1},
  seal: {_id: 'seal', type: 'seal', chance: 0},
  firearm: {_id: 'firearm', type: 'combat', chance: 23},
  phylactery: {_id: 'phylactery', type: 'magic', chance: 0},
}

var weapons = 'firearm dynamite'.split(' ')
var types = 'combat investigation magic recovery resource seal'.split(' ')
var Tile = new Schema({
  _id: { type: String },
  type: { type: String },
  chance: Number
});

module.exports.Lookups = tiles
module.exports.Tile = Tile