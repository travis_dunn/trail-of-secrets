var CONFIG = require('config');
var mongoose = require('mongoose');
var Schema = mongoose.Schema

var powers = {
  as_above_so_below: {_id: 'as_above_so_below', name: 'As Above, So Below', magic: 10, description: 'Mirror swap the top and bottom halves of the board'},
  body_warping_of_gorgoroth: {_id: 'body_warping_of_gorgoroth', name: 'Body Warping of Gorgoroth', magic: 15, description: 'Tranform monster into new random monster'},
  faustian_riches: {_id: 'faustian_riches', name: 'Faustian Riches', magic: 15, description: 'Collect all resource tiles on the board'},
  flesh_ward: {_id: 'flesh_ward', name: 'Flesh Ward', magic: 15, description: 'Collect all recovery tiles on the board'},
  the_great_work: {_id: 'the_great_work', name: 'The Great Work', magic: 10, description: 'Upgrade basic tile to ehanced tile'},
  legerdemain: {_id: 'legerdemain', name: 'Legerdemain', magic: 5, description: 'Swap position of two chosen tiles'},
  supernal_binding: {_id: 'supernal_binding', name: 'Supernal Binding', magic: 5, description: 'Pick a monster to skip its next turn'},
  solomons_wisdom: {_id: 'solomons_wisdom', name: "Solomon's Wisdom", magic: 20, description: 'Pick a cell and mirror swap the row and column'},
  ley_line_convergence: {_id: 'ley_line_convergence', name: 'Ley Line Convergence', magic: 15, description: 'Collect all magic tiles on the board'},
  chant_of_thoth: {_id: 'chant_of_thoth', name: 'Chant of Thoth', magic: 10, description: 'Double chance of collecting bonus tiles on the next match'},
  call_the_beast: {_id: 'call_the_beast', name: 'Call the Beast', magic: 8, description: 'Pick a monster to send to the top of the board'}
}
function powerList(){
  var a = []
  for (var key in powers) { a.push(powers[key]) }
  return a
}

var dictionary = {
  _id: String,
  description: String,
  level: {type: Number, default: 1},
  magic: Number,
  name: String
}
var Power = new Schema(dictionary);

Power.statics.menu = function (investigator) {
  function isPowerAvailable(powerId) {
    for (var i = 0; i < investigator.powers.length; i++) {
      if (investigator.powers[i]._id === powerId) {
        return investigator.powers[i].level < CONFIG.Powers.maxLevel
      }
    }
    return true
  }

  // Build a menu of three random, unique new or upgrade powers, based on investigator's current powers
  var m = []
  var a = powerList()

  for (var i = a.length-1; i >= 0; i--) {
    if (!isPowerAvailable(a[i]._id)) a.shift(i, 1)
  }

  for (var i = 0; i < CONFIG.Powers.menuLength; i++) {
    var num = Math.floor(Math.random() * a.length)
    var power = a[num]
    if (investigator.power(power._id)) {
      power.description = "Upgrade to level "+(investigator.power(power._id).level + 1)
    }
    m.push(power)
    a.splice(num, 1)
  }
  return m
}

module.exports.Power = Power
module.exports.Dictionary = dictionary
module.exports.Lookups = powers