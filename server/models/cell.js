var monster = require('./monster');
var plot = require('./plot');
var tiles = require('./tile').Lookups;
var mongoose = require('mongoose');
var Schema = mongoose.Schema

var dictionary = {
  _id: Number,
  monster: { type: monster.Dictionary, get: monster.Cast},
  plot: { type: plot.Dictionary, get: plot.Cast},
  tileId: String
}
var Cell = new Schema(dictionary);

Cell.methods.hasMonster = function () {
  return this.tileId && this.monster ? true : false
}
Cell.methods.hasPlot = function () {
  return this.tileId && this.plot ? true : false
}
Cell.methods.tileType = function () {
  return this.tileId ? tiles[this.tileId].type : null
}
Cell.methods.isCleared = function () {
  return this.tileId ? false : true
}
Cell.methods.isEvenRow = function (size) {
  return this.y(size) % 2 !== 0
}
Cell.methods.x = function (size) {
  return this._id % (size || 7)
}
Cell.methods.y = function (size) {
  return Math.floor(this._id / (size || 7))
}
Cell.methods.isAdjacentTo = function(cell) {
  return Math.abs(cell.x() - this.x()) < 2 && Math.abs(cell.y() - this.y()) < 2
}
Cell.methods.serialize = function () {
  var serialObject = {tileId: this.tileId, tileType: this.tileType() }
 
  if (this.hasMonster()) serialObject.monster = this.monster.serialize()
  if (this.hasPlot()) serialObject.plot = this.plot.serialize()
  return serialObject
}
Cell.methods.empty = function () {
  this.plot = null
  this.monster = null
  this.tileId = null
}
Cell.methods.clear = function () {
  if (this.hasMonster()) {
    if (this.monster.isDead()) {
      this.monster = null
      this.tileId = null
    }
  } else if (this.hasPlot()) {
    if (this.plot.isSolved()) {
      this.plot = null
      this.tileId = 'seal'
    }
  } else {
    this.tileId = null
  }
}
Cell.methods.attackMonster = function (cells, delta, turn) {
  if (!this.hasMonster()) return

  var monster = this.monster

  var defense = monster.defense
  if (monster._id === 'cultist') {
    // high priests double cultist defense 
    var index = this._id
    var hasHighPriests = cells.filter(function(c){ return c.tileId === 'high_priest' }).length > 0
    if (hasHighPriests) defense = defense * 2
  } else if (monster._id === 'ghost') {
    // ghosts can only be damaged on odd turns
    var isEvenTurn = (turn % 2 !== 0)
    if (isEvenTurn) delta = 0
  } else if (monster._id === 'tcho_tcho') {
    // tcho-tcho cannot be damaged until it is the last monster left
    var hasNonTchoTcho = cells.filter(function(c){ return c.tileId !== 'tcho_tcho' }).length > 0
    if (hasNonTchoTcho) delta = 0
  } else if (monster._id === 'deep_one') {
    // deep ones gain bonus for how far down their tile is on the board
    defense += this.y()
  } else if (monster._id === 'dark_young' && this.isEvenRow()) {
    // dark young cannot be damaged on even rows
    delta = 0
  }

  var damage = Math.max(delta - defense, 0)
  monster.health = Math.max( monster.health - damage, 0)
  this.monster = monster
  return damage
}
Cell.methods.solvePlot = function (delta) {
  if (!this.hasPlot()) return

  var plot = this.plot
  plot.clues = Math.min( Math.max(plot.clues + delta, 0), plot.level)
  this.plot = plot
}

module.exports.Cell = Cell
module.exports.Dictionary = dictionary