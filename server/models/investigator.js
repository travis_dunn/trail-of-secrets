var CONFIG = require('config');
var mongoose = require('mongoose');
var Insanity = require('./insanity').Insanity;
var item = require('./item');
var Item = mongoose.model('Item');
var power = require('./power');
var Schema = mongoose.Schema

var cast = function (val) {
  if (!val) return null;
  var I = mongoose.model('Investigator');
  investigator = new I(val)
  return investigator
}

var health = {
  current: {type: Number, default: 35},
  total: {type: Number, default: 35}
}
var sanity = {
  current: {type: Number, default: 25},
  total: {type: Number, default: 25}
}
var wards = {
  current: {type: Number, default: 0},
  total: {type: Number, default: 5}
}
var magic = {
  current: {type: Number, default: 0},
  total: {type: Number, default: 10}
}
var skills = {
  adventure: {type: Number, default: 1}, // health defense
  conflict: {type: Number, default: 1}, // combat damage
  occult: {type: Number, default: 1}, // sanity defense
  research: {type: Number, default: 1}, // chances for bonus tile per tile collected
  hunting: {type: Number, default: 5}, // % chance of bonus tile per combat tile collected
  healing: {type: Number, default: 5}, // % chance of bonus tile per recovery tile collected 
  funding: {type: Number, default: 5}, // % chance of bonus tile per resource tile collected 
  augury: {type: Number, default: 5}, // % chance of bonus tile per magic tile collected 
  arcana: {type: Number, default: 5} // % chance of bonus tile per investigation tile collected 
}

var dictionary = {
  created: { type: Date, default: Date.now },
  difficulty: { type: Number, default: 0 },
  equipment: [item.Item],
  experience: { type: Number, default: 0 },
  health: health,
  insanities: [Insanity],
  level: { type: Number, default: 1 },
  magic: magic,
  name: { type: String, default: 'Titus Crow' },
  powers: { type: [power.Power], default: [power.Lookups.legerdemain] },
  sanity: sanity,
  score: { type: Number, default: 0 },
  skills: skills,
  wards: wards,
  wealth: { type: Number, default: 0 },
}
var Investigator = new Schema(dictionary);

Investigator.methods.levelExperience = function (level) {
  var exp = CONFIG.Advancement.baseLevelExperience
  var multiplier = 1 + CONFIG.Advancement.experienceMultiplier

  var total = exp
  for (var i = 1; i < level; i++) {
    exp = exp * multiplier // every level requires X% more experience than the last
    total += exp
  }
  return Math.ceil(total)
}
Investigator.methods.nextLevelExperience = function () {
  return this.levelExperience(this.level)
}
Investigator.methods.isInsane = function () {
  return this.sanity.current === 0
}
Investigator.methods.power = function (powerId) {
  var power = this.powers.filter(function(p){ return p._id === powerId })
  return power.length ? power[0] : null
}
Investigator.methods.insanity = function (insanityId) {
  if (!this.insanities) return

  for (var i = 0; i < this.insanities.length; i++) {
    if (this.insanities[i]._id === insanityId) return this.insanities[i]
  }
}
Investigator.methods.hasAdvancedLevel = function () {
  return this.experience >= this.nextLevelExperience()
}
Investigator.methods.isDead = function () {
  return this.health.current === 0
}
Investigator.methods.hasEquipment = function (itemId) {
  return (this.accessory() && this.accessory()._id === itemId) ||
         (this.attire() && this.attire()._id === itemId) ||
         (this.weapon() && this.weapon()._id === itemId)
}
Investigator.methods.accessory = function () {
  for (var i = 0; i < this.equipment.length; i++) {
    if (this.equipment[i].type === 'accessory') return this.equipment[i]
  }
}
Investigator.methods.attire = function () {
  for (var i = 0; i < this.equipment.length; i++) {
    if (this.equipment[i].type === 'attire') return this.equipment[i]
  }
}
Investigator.methods.weapon = function () {
  for (var i = 0; i < this.equipment.length; i++) {
    if (this.equipment[i].type === 'weapon') return this.equipment[i]
  }
}
Investigator.methods.modifier = function (modifierId) {
  var mod = 0
  var accessory = this.accessory()
  var attire = this.attire()
  var weapon = this.weapon()

  if (accessory) mod += accessory.modifier(modifierId)
  if (attire) mod += attire.modifier(modifierId)
  if (weapon) mod += weapon.modifier(modifierId)

  return mod
}
Investigator.methods.damage = function (matches) {
  var damage = this.skills.conflict

  if (this.equipment.accessory) {
    damage += this.equipment.accessory.modifier('conflict')
    damage += this.equipment.accessory.modifier('weapons') * matches
  }
  if (this.equipment.attire) {
    damage += this.equipment.attire.modifier('conflict')
    damage += this.equipment.attire.modifier('weapons') * matches
  }
  console.log(JSON.stringify(this.equipment.weapon))
  if (this.equipment.weapon) {
    damage += this.equipment.weapon.modifier('conflict')
    damage += this.equipment.weapon.modifier('weapons') * matches
    console.log("this.equipment.weapon.modifier('conflict') "+this.equipment.weapon.modifier('conflict'))
    console.log("this.equipment.weapon.modifier('weapons') "+this.equipment.weapon.modifier('weapons'))
  }

  return damage
}
Investigator.methods.modifyWealth = function (delta) {
  this.wealth = Math.max(this.wealth + delta, 0)
}
Investigator.methods.modifyHealth = function (delta) {
  var old = this.health.current
  this.health.current = Math.max( Math.min(this.health.current + delta, this.health.total), 0)
  return this.health.current - old
}
Investigator.methods.modifySanity = function (delta) {
  var old = this.sanity.current
  this.sanity.current = Math.max( Math.min(this.sanity.current + delta, this.sanity.total), 0)
  return this.sanity.current - old
}
Investigator.methods.modifyWards = function (delta) {
  this.wards.current = Math.max( Math.min(this.wards.current + delta, this.wards.total), 0)
}
Investigator.methods.gainMagic = function (delta) {
  if (delta === 0) return
  this.magic.current = Math.max( Math.min(this.magic.current + delta, this.magic.total), 0)
}
Investigator.methods.gainRites = function (delta) {
  var totalWards = this.wards.total

  if (this.wards.current + delta > totalWards) {
  	var rollover = delta - (totalWards - this.wards.current)
    this.gainMagic(rollover)
    this.wards.current = totalWards
    return [delta - rollover, rollover]
  } else {
    this.wards.current = Math.max( Math.min(this.wards.current + delta, totalWards), 0)
    return [delta, 0]
  }
}
Investigator.methods.restore = function () {
  this.health.current = this.health.total
  this.sanity.current = this.sanity.total
}
Investigator.methods.serializeExp = function () {
  var exp = this.level > 1 ? this.levelExperience(this.level - 1) : 0
  exp = {
    current: this.experience-exp,
    next: this.nextLevelExperience()-exp,
  }
  return exp
}
Investigator.methods.serialize = function () {
  var serialObject = {
    experience: this.serializeExp(),
  	health: this.health, 
  	level: this.level, 
  	magic: this.magic,
  	name: this.name, 
    powers: this.powers, 
  	sanity: this.sanity,
  	skills: this.skills,
  	wards: this.wards,
  	wealth: this.wealth 
  }
  serialObject.equipment = {
    accessory: this.accessory(),
    attire: this.attire(),
    weapon: this.weapon(),
  }
  if (this.insanities && this.insanities.length > 0) {
    serialObject.insanities = []
    for (var i = 0; i < this.insanities.length; i++) { 
      serialObject.insanities.push(this.insanities[i].serialize()) 
    }
  }
  return serialObject
}

module.exports.Investigator = Investigator
module.exports.Dictionary = dictionary
module.exports.Cast = cast