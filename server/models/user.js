var mongoose = require('mongoose');
var score = require('./score');
var Schema = mongoose.Schema

var facebook = {
  userId: String,
  fullName: String,
  username: String
}

var dictionary = {
  name: String,
  created: { type: Date, default: Date.now },
  facebook: facebook,
  patronage: { type: Number, default: 10 },
  highScore: score.Dictionary,
  gameId: String
}
var User = new Schema(dictionary);

User.index({"facebook.userId'": 1})

User.statics.findFacebook = function (facebookId, cb) {
  this.findOne({ 'facebook.userId': facebookId}, cb)
}
User.statics.register = function (facebookUserId, gameId, cb) {
  var U = mongoose.model('User')
  var user = new U()
  user.facebook.userId = facebookUserId
  user.gameId = gameId
  user.save(cb)
}
User.statics.setUserGame = function (userId, gameId, cb) {
  this.update({_id: userId}, {$set: { gameId: gameId }}, cb)
}

User.methods.modifyPatronage = function (delta) {
  this.patronage = Math.max(this.patronage+delta, 0)
}
User.methods.serialize = function () {
  var serialObject = {
    name: this.name,
    gameId: this.gameId,
    patronage: this.patronage,
    facebookId: this.facebook ? this.facebook.userId : null
  }
  return serialObject
}

module.exports.User = User
module.exports.Dictionary = dictionary