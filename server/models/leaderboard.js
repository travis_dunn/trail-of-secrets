var CONFIG = require('config');
var mongoose = require('mongoose');
var Score = mongoose.model('Score');
var Schema = mongoose.Schema

var leaderboardLength = 25
var score = {
  name: String,
  level: Number,
  score: Number
}

function today() {
  var d = new Date()
  d.setHours(0,0,0,0)
  return d
}

var LEADERBOARD_TYPES = 'kills score wealth experience turns matches'.split(' ')

var dictionary = {
  created: { type: Date, default: Date.now },
  name: String,
  patronage: {type: Boolean, default: false},
  period: String,
  scores: [score],
  type: {type: String, enum: LEADERBOARD_TYPES},
  timestamp: {type: Number, default: 0}
}
var Leaderboard = new Schema(dictionary);

Leaderboard.index({"created": -1})
Leaderboard.index({'type': 1, 'period': 1})

Leaderboard.statics.lastWeek = function () {
  var oneWeekAgo = today()
  oneWeekAgo.setDate(-7);

  var filter = { created: {$gte: oneWeekAgo }}
  return this.find(filter)
}
Leaderboard.statics.lastMonth = function () {
  var oneMonthAgo = today()
  oneMonthAgo.setDate(-30);

  var filter = { created: {$gte: oneMonthAgo }}
  return this.find(filter)
}

Leaderboard.statics.findUser = function (userId, cb) {
  var leaderboard = new Leaderboard({name: 'Personal', period: 'all', type: 'personal', timestamp: 1})

  Score.limit(leaderboardLength).sort({level: 'desc'}).findOne({userId: userId}, function(err, scores) {
    leaderboard.scores = scores || []
    cb(err, leaderboard)
  })
}
Leaderboard.statics.findBoard = function (type, period, cb) {
  Leaderboard.sort({timestamp: 'desc'}).findOne({type: type, period: period}, cb)
}

Leaderboard.statics.awardRankedUsers = function (type, period, scores) {
  var level = type === 'score' ? 'primary' : 'secondary'
  var awards = CONFIG.Leaderboards.rewards[level][period]
  var count = Math.min(awards.length, scores.length)

  for (var i = 0; i < count; i++) {
    if (scores.length)
    var userId = scores[i].userId
    // if a registered user ranked then award them patronage
    if (userId) {
      var p = awards[i]
      User.update({_id : userId}, {$inc: { patronage : p}}, function(err){ })            
    }
  }
}

Leaderboard.statics.rankBoard = function (type, name, cb) {
  var self = this
  Score.recent(type, leaderboardLength).exec(function(err, docs) {
    var leaderboard = new self({name: name, period: 'daily', type: type, timestamp: 1})
    var ids = []
    if (docs && docs.length > 0) {
      docs.forEach( function(i){
        var score = {name: 'username', score: i[type]}
        if (i.userId) score.userId = i.userId
        leaderboard.scores.push(score)
        ids.push(i._id)
      })

      // flag scores as having ranked on the leaderboards
      Score.update({_id: {$in: ids }}, {$set: {ranked: true }}) 

      leaderboard.save(cb)
      self.awardRankedUsers(type, 'daily', leaderboard.scores)
    } else {
      cb()
    }
  })
}


Leaderboard.statics.ifHasNotRankedToday = function (cb) {
  // if leaderboard exists then it was already calculated for today, so don't invoke callback
  this.findOne({created: {$gte: today(), period: 'daily'}}, function(err, doc) {
    if (!doc) cb()
  })
}

Leaderboard.statics.ifHasNotRankedThisWeek = function (cb) {
  var lastWeek = today()
  lastWeek.setDate(lastWeek.getDate()-7);
  this.findOne({created: {$gte: lastWeek, period: 'weekly'}}, function(err, doc) {
    if (!doc) cb()
  })
}

Leaderboard.statics.ifHasNotRankedThisMonth = function (cb) {
  var lastMonth = today()
  lastMonth.setDate(lastMonth.getDate()-30);
  this.findOne({created: {$gte: lastMonth, period: 'monthly'}}, function(err, doc) {
    if (!doc) cb()
  })
}

Leaderboard.statics.rankToday = function (cb) {
  var self = this

  this.ifHasNotRankedToday(function(err, doc) {

    // track independent ranking methods and invoke callback once all are finished (i.e. a poor man's promise)
    var count = 5
    function finish() {
      count--
      if (count === 0) cb()
    }

    self.rankBoard('score', 'High Score', finish)
    self.rankBoard('kills', 'Most Kills', finish)
    self.rankBoard('wealth', 'Greatest Wealth', finish)
    self.rankBoard('turns', 'Endurance', finish)
    self.rankBoard('experience', 'Experience', finish)
  })
}
Leaderboard.statics.rankThisWeek = function (cb) {
  var self = this

  this.ifHasNotRankedThisWeek(function(err, doc) {
    // track independent ranking methods and invoke callback once all are finished
    var count = LEADERBOARD_TYPES.length
    function finish() {
      count--
      if (count === 0) cb()
    }

    console.log("## Ranking weekly leaderboards")

    for (var i = 0; i < LEADERBOARD_TYPES.length; i++) {
      var type = LEADERBOARD_TYPES[i]
      var scores = []

      // create a weekly leaderboard of the best daily scores from last week
      self.lastWeek().find({type: type, period: 'daily'}, function(err, leaderboards) {
        var name = type
        for (var l = 0; l < leaderboards.length; l++) { 
          var leaderboard = leaderboards[l]
          name = leaderboard.name
          type = leaderboard.type
          for (var i = 0; i < leaderboard.scores.length; i++) { 
            scores.push(leaderboard.scores[i])
          }
        }

        var leaderboard = new self({name: name, period: 'weekly', type: type, timestamp: 1})
        leaderboard.created = today()

        // take the top 25 daily scores
        scores.sort(function(a, b) { return b[type] - a[type] })
        leaderboard.scores = scores.splice(0, leaderboardLength)

        leaderboard.save(finish)
        self.awardRankedUsers(type, 'weekly', leaderboard.scores)
      })

    }
  })

}
Leaderboard.statics.rankThisMonth = function (cb) {
  var self = this

  this.ifHasNotRankedThisMonth(function(err, doc) {
    // track independent ranking methods and invoke callback once all are finished
    var count = LEADERBOARD_TYPES.length
    function finish() {
      count--
      if (count === 0 && typeof(cb) === "function") cb()
    }

    for (var i = 0; i < LEADERBOARD_TYPES.length; i++) {
      var type = LEADERBOARD_TYPES[i]
      var scores = []

      // create a monthly leaderboard of the best weekly scores from last month
      self.lastMonth().find({type: type, period: 'weekly'}, function(err, leaderboards) {
        var name = type
        for (var l = 0; l < leaderboards.length; l++) { 
          var leaderboard = leaderboards[l]
          name = leaderboard.name
          type = leaderboard.type
          for (var i = 0; i < leaderboard.scores.length; i++) { 
            scores.push(leaderboard.scores[i])
          }
        }

        var leaderboard = new self({name: name, period: 'monthly', type: type, timestamp: 1})
        leaderboard.created = today()        

        // take the top 25 weekly scores
        scores.sort(function(a, b) { return b[type] - a[type] })
        leaderboard.scores = scores.splice(0, leaderboardLength)
        
        leaderboard.save(finish)
      })

    }
  })
}
Leaderboard.statics.rank = function () {
  var self = this
  this.collection.drop(function(){ 
    // update the various ranking periods; 
    // callback will not be invoked if ranking was unnecessary, so the rank method is idempotent
    self.rankToday(function() {
      self.rankThisWeek(function() {
        self.rankThisMonth()
      })
    })
  });
}

module.exports.Leaderboard = Leaderboard
module.exports.Dictionary = dictionary
