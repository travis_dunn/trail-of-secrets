var dealer = require('./dealer')
var monsters = require('./../models/monster').Lookups;
var logger = require('./../logger')

function hasStarSpawn(board) {
  for (var i = 0; i < board.length; i++) {
    var tuple = board[i]
    if (typeof tuple[0] === 'Object') {
      if (tuple.monster && tuple.monster._id === 'star_spawn') return true
    }
  }
  return false
}

exports.moveTile = function(game, fromIndex, toIndex) {
  game.cells[toIndex].tileId = game.cells[fromIndex].tileId
  game.cells[toIndex].monster = game.cells[fromIndex].monster
  game.cells[toIndex].plot = game.cells[fromIndex].plot
  game.cells[fromIndex].empty()
}
exports.retile = function(game, index) {
  var cell = dealer.newCell(game, index)

  game.cells[index].tileId = cell.tileId
  game.cells[index].monster = cell.monster
  game.cells[index].plot = cell.plot
}
exports.nextTiledCellAbove = function(game, index) {
  var startingIndex = index - dealer.BOARD_SIZE

  for (var i = startingIndex; i >= 0; i-=dealer.BOARD_SIZE) {
  	var cell = game.cells[i]
  	if (!cell.isCleared()) return cell
  }
}
exports.reseatCell = function(game, cell, turn) {
  var index = cell._id

  // if at the top of the board add a new cell and return
  if (cell.y() === 0) {
    exports.retile(game, index)
    if (turn) turn.reseatTile( game.cells[index].serialize(), index )
    return
  }

  var nextCell = exports.nextTiledCellAbove(game, index)
  if (nextCell) {
    exports.moveTile(game, nextCell._id, index)
    if (turn) turn.reseatTile( nextCell._id, index )

    // if next cell only moved down one cell, then keep shifting the cells
    if (nextCell._id === index-1) {
      exports.reseatCell(game, nextCell, turn)
    }
  } else { 
    // no next cell above so drop a new cell in
    exports.retile(game, index)
    if (turn) turn.reseatTile( game.cells[index].serialize(), index )
  }
}

exports.reseat = function(game, turn) {
  for (var i = game.cells.length-1; i >= 0; i--) {
  	var cell = game.cells[i]
  	if (cell.isCleared()) {
      exports.reseatCell(game, cell, turn) 
  	}
  }
  if (!turn) return

  if (hasStarSpawn(turn.board)) {
    console.log("HAS STAR SPAWN ("+turn.board.length+")")
    for (var i = 0; i < turn.board.length; i++) {
      var tuple = turn.board[i]
      // star spawn turn all non-monster cells into cultists when they enter
      console.log("tuple = "+JSON.stringify(tuple))
      if (typeof tuple[0] === 'Object') {
        if (!tuple.monster) {
          var index = tuple[1]
          game.cells[index].tileId = 'monster'
          game.cells[index].monster = monsters['cultist']
          game.cells[index].plot = null
          turn.board[i][0] = game.cells[index].serialize()
        }
      }
    }
    console.log(JSON.stringify(turn.board))
  }
}