var CONFIG = require('config');
var mongoose = require('mongoose');
var Monster = mongoose.model('Monster');
var Tile = mongoose.model('Tile');
var Cell = mongoose.model('Cell');
var Plot = mongoose.model('Plot');
var tiles = require('./../models/tile').Lookups;
var monsters = require('./../models/monster').Lookups;

exports.BOARD_SIZE = BOARD_SIZE = 7

exports.availableTiles = function(turn, monsterCount) {
  var isFirstTurn = turn === 1 ? true : false
  var atMaxMonsters = monsterCount >= CONFIG.Dealing.maxFirstTurnMonsters
  var available = []

  for (var key in tiles) {
    if (!tiles.hasOwnProperty(key)) continue
    var tile = tiles[key]
    var isDealable = tile.chance > 0
    var ignoringRecovery = isFirstTurn && tile.type === 'recovery'
    var ignoringMonsters = isFirstTurn && atMaxMonsters && tile._id === 'monster'

    if (isDealable && !ignoringRecovery && !ignoringMonsters) {
      available.push(tile)
    }
  }
  return available
}

exports.newTile = function(available) {
  var total = 0
  for (var i = 0; i < available.length; i++) total += available[i].chance
  var num = Math.floor(Math.random() * total)

  var cursor = 0
  var newTile = null
  for (var i = 0; i < available.length; i++) {
    var tile = available[i]
    if (num >= cursor && num < cursor + tile.chance) {
      newTile = new Tile(tile)
      return newTile
    }
    cursor += tile.chance
  }
}
exports.newMonster = function(game, monsterId) {
  function randomChance(list) {
    var total = 0
    for (var i = 0; i < list.length; i++) { total += list[i].chance}
    return Math.floor(Math.random() * total)
  }

  function monsterHealthBoost(monster) {
    var boosts = Math.floor(game.investigator.level / CONFIG.Difficulty.monsterLevelBoostInterval)
    var boost = boosts * CONFIG.Difficulty.monsterLevelBoost

    return Math.ceil(boost * monster.health)
  }

  // monsters appear earlier in game at higher difficulties
  var baseDifficulty = game.greatOldOne().difficulty - 1
  var turnAppearanceModifier = Math.pow(CONFIG.Difficulty.turnMultiplier, baseDifficulty)
  function canAppearThisTurn(monster) {
    var appearsOnTurn = Math.floor(monster.turn * turnAppearanceModifier)
    return appearsOnTurn <= game.turn()
  }

  // exclude monsters of too high a level or, if specific monster given, only that monster
  var m = []
  for (var key in monsters) {
    var monster = monsters[key]

    if (monsterId && monster._id !== monsterId) {
      m.push(monster)
    } else if (canAppearThisTurn(monster)) {
      m.push(monster)
    }
  }

  var num = randomChance(m)
  var cursor = 0

  for (var i = 0; i < m.length; i++) {
    var monster = m[i]
    if (num >= cursor && num < cursor + monster.chance) {
      monster = new Monster(monster)

      // yithians inherit stats from investigator
      if (monster._id === 'yithian') {
        if (game.investigator) {
          monster.damage = game.investigator.skills.conflict
          monster.terror = game.investigator.skills.occult
          monster.defense = (game.investigator.wards.current * 2)
        } else {
          monster.damage = 1
          monster.terror = 1
        }
      }
   
      // monsters receive a boost to base health based on investigator level
      var boost = monsterHealthBoost(monster)
      monster.health += monsterHealthBoost(monster)
      
      return monster.serialize()
    }
    cursor += monster.chance
  }
}
exports.newPlot = function() {
  var plots = Plot.list()
  var num = Math.floor(Math.random() * plots.length)
  var plot = plots[num]
  plot.clues = 0
  return plot
}
exports.newCell = function(game, index) {
  var availableTiles = exports.availableTiles(game.turn(), game.monsterCells().length)
  var tile = exports.newTile(availableTiles)
  var monster = tile._id === 'monster' ? exports.newMonster(game) : null
  var plot = tile._id === 'plot' ? exports.newPlot() : null
  return new Cell({_id: index, tileId: tile._id, monster: monster, plot: plot})
}
exports.newGameboard = function(game) {
  var board = []
  for (y = 0; y < BOARD_SIZE; y++) {
    for (x = 0; x < BOARD_SIZE; x++) {
      var index = (y * BOARD_SIZE) + x
  	  var cell = new exports.newCell(game, index)

      board.push(cell)
    }
  }

  return board
}