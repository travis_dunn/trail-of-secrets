var CONFIG = require('config');

exports.highScore = function(game) {
  var score = 0
  score += game.stats.moves * CONFIG.Scoring.pointsPerMove
  score += game.seals.current * CONFIG.Scoring.pointsPerSeal
  score += game.stats.monsters * CONFIG.Scoring.pointsPerKill
  score += game.stats.experience * CONFIG.Scoring.experienceMultiplier

  score += CONFIG.Scoring.difficulty.pointsPerLevel * 
           ((game.greatOldOne().difficulty-1) * CONFIG.Scoring.difficulty.levelMultiplier)

  score = game.isSealed() ? (score * CONFIG.Scoring.victoryMultiplier) : score

  return Math.floor(score)
}