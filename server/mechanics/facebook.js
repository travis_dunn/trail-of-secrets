var mongoose = require('mongoose');
var crypto = require("crypto");
var CONFIG = require('config');
var logger = require("./../logger");

function decodeBase64(payload) {
  return new Buffer(payload, 'base64').toString('ascii')
}

function getItemInfo(itemId) {
  var info = {}
  var item = mongoose.model('Item').lookup(itemId)
  if (item) {
    info = {
      content: [{
        title: item.name,
        description: item.description,
        price: item.cost,
        item_id: itemId,
        image_url: 'https://'+CONFIG.Server.CDN+'/images/products/currency.png',
        product_url: 'https://'+CONFIG.Server.CDN+'/images/products/currency.png'
      }],
      method: "payments_get_items"
    }
  }
  return info 
}
function getOrderPayment(orderId, placed) {
  var status = placed === false ? 'canceled' : 'settled'
  var info = {
    content: {
      status: status,
      order_id: orderId
    },
    method: "payments_status_update"
  }
  return info
}

exports.verifySignature = function(signedRequest) {
  if (!signedRequest) return null

  var encodedSig = signedRequest.split('.')[0]
  var payload = signedRequest.split('.')[1]

  var decodedSig = decodeBase64(encodedSig)
  var decodedPayload = JSON.parse(decodeBase64(payload))

  var expectedSig = crypto.createHmac('sha256', CONFIG.Facebook.secret).update(payload).digest('base64')
  var facebookSig = encodedSig.replace(/_/g,"/").replace(/-/g,"+")+'=' // facebook has custom symbols in their algo
  var isMatchingSig = expectedSig === facebookSig

  return isMatchingSig ? decodedPayload : null
}

exports.paymentCallback = function(req, cb) {
  var userPayload = exports.verifySignature(req.body.signed_request)

  logger.log('app', 'FACEBOOK Payment Callback Payload', {'payload': userPayload, 'body': req.body})
  
  if (userPayload) {
    var method = req.body.method

    // REF: https://developers.facebook.com/docs/payments/callback/#payments_get_items
    if (method === 'payments_get_items') {
      var orderInfo = JSON.parse( userPayload.credits.order_info )
      var itemId = orderInfo.itemId

      var data = getItemInfo(itemId)
      logger.log('app', 'FACEBOOK payments_get_items Developer Response', {'reponse': data, 'itemId': itemId})
      cb(null, data)
    // REF: https://developers.facebook.com/docs/payments/callback/#payments_status_update
    } else if (method === 'payments_status_update') {
      var userId = userPayload.user_id
      var status = userPayload.credits.status
      var orderId = userPayload.credits.order_id

      if (status === 'placed') {
        var details = JSON.parse(userPayload.credits.order_details)
        var itemId = details.items[0].item_id

        mongoose.model('User').findFacebook(userId, function(err, user) {
          if (user) {
            var item = mongoose.model('Item').lookup( itemId )
            item.purchase(user)
            logger.log('app', 'FACEBOOK payments_status_update placed! '+JSON.stringify(orderInfo))

            user.save(function(err) {
              cb(null, getOrderPayment(orderId, err ? false : true))
            })
          } else {
            cb('no user id '+userPayload.user_id, getOrderPayment(orderId, false))
          }
        })
      } else {
        cb('status was '+status, getOrderPayment(orderId, false))
      }
    } else {
      cb()
    }
  } else {
    cb(false)
  }
}