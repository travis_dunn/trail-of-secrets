var mongoose = require('mongoose');
var Advancement = mongoose.model('Advancement');

function advancementFromMenu(menu, id) {
  if (!menu) return

  var advancements = menu.advancements
  for (var i = 0; i < advancements.length; i++) {
    if (advancements[i]._id === id) return advancements[i]
  }
}

exports.empower = function(game, powerId) {
  if (!game.advancements || game.advancements.length != 1) return

  var menu = game.advancements[0]
  var advancement = advancementFromMenu({advancements: menu.powers}, powerId)
  if (!advancement) return

  var investigator = game.investigator
  var power = investigator.power(powerId)

  // if power is already possessed then increase the level
  if (power) {
    power.level++
    power.magic = Math.max(power.magic-3, 5) // decrease cost by 3 (per level), to a minimum of 5
  } else {
    // if power is not possessed, then add it from available powers menu
    power = menu.powers.filter(function(p) { return p._id === powerId })[0]
    if (power) {
      // max 4 powers, so push out last power
      if (investigator.powers.length > 3) investigator.powers.splice(3, 1)
      investigator.powers.push(power)
    }
  }

  game.investigator = investigator
  game.advancements.splice(0,1) // remove power menu now that it has been used

  return power
}

exports.advance = function(game, advancementId) {
  if (!game.advancements || game.advancements.length < 1) return

  var menu = game.advancements[0]
  var advancement = advancementFromMenu(menu, advancementId)
  if (!advancement) return

  var results = {}
  var investigator = game.investigator
  var delta = parseInt(advancement.delta)
  switch(advancement._id) {
    case 'conflict':
      investigator.skills.conflict += delta
      results.conflict = delta
      break
    case 'health':
      investigator.health.total += delta
      investigator.health.current += delta
      results.health = delta
      break
    case 'sanity':
      investigator.sanity.total += delta
      investigator.sanity.current += delta
      results.sanity = delta
      break
    case 'occult':
      investigator.skills.occult += delta
      results.occult = delta
      break
    case 'research':
      investigator.skills.research += delta
      results.research = delta
      break
    case 'wards':
      investigator.wards.total += delta
      investigator.wards.current += delta
      results.wards = delta
      break
    case 'magic':
      investigator.magic.total += delta
      investigator.magic.current += delta
      results.magic = delta
      break
    case 'adventure':
      investigator.skills.adventure += delta
      results.adventure = delta
      break
    case 'hunting':
      investigator.skills.hunting += delta
      results.hunting = delta
      break
    case 'healing':
      investigator.skills.healing += delta
      results.healing = delta
      break
    case 'funding':
      investigator.skills.funding += delta
      results.funding = delta
      break
    case 'augury':
      investigator.skills.augury += delta
      results.augury = delta
      break
    case 'arcana':
      investigator.skills.arcana += delta
      results.arcana = delta
      break
  }

  game.investigator = investigator
  game.advancements.splice(0,1) // remove advancement menu now that it has been used
  return results
}

exports.tally = function(game, turn) {
  var investigator = game.investigator
  var didAdvance = investigator.hasAdvancedLevel()

  // supplies restock each new level
  if (didAdvance) {
    game.resupply()
    turn.supply(game.supplies)
  } 

  // add advancement menus for each level advanced (it's possible to advance multiple levels in a single move)
  while (investigator.hasAdvancedLevel()) {
  	var menu = {level: investigator.level+1, advancements: Advancement.menu()}
    console.log('advanced to level '+(investigator.level+1))
  	investigator.level++
    game.stats.levels++
    game.advancements.push(menu)
    turn.advance(menu)
  }
  game.investigator = investigator

  // serialize new experience requirements
  if (didAdvance) turn.experience( investigator.serializeExp() )
}