var board = require('./board')
var matcher = require('./matcher')
var dealer = require('./dealer')
var Turn = require('./turn').Turn

exports.playAsAboveSoBelow = function(game, power) {
  var investigator = game.investigator
  investigator.magic.current = Math.max(investigator.magic.current - power.magic, 0)
  game.investigator = investigator

  for (var y = 4; y < 7; y++) {
    for (var x = 0; x < 7; x++) {
      var index = game.indexFromCoordinates(x, y)
      var mirrorIndex = game.indexFromCoordinates(x, 6-y)

      game.swapCells(index, mirrorIndex)
    }
  }

  var results = {flip: 'all'}
  return results
}
exports.playFaustianRiches = function(game, power) {
  var resources = []
  for (var i = 0; i < game.cells.length; i++) { 
    var cell = game.cells[i]
    if (cell.tileType() === 'resource') resources.push(cell._id)
  }
  var turn = new Turn()

  matcher.matchResources(game, turn, resources)
  matcher.clearMatches(game, resources)
  board.reseat(game, turn)

  return turn.serialize()
}
exports.playFleshWard = function(game, power) {
  var resources = []
  for (var i = 0; i < game.cells.length; i++) { 
    var cell = game.cells[i]
    if (cell.tileType() === 'recovery') resources.push(cell._id)
  }
  var turn = new Turn()

  matcher.matchRecovery(game, turn, resources)
  matcher.clearMatches(game, resources)
  board.reseat(game, turn)

  return turn.serialize()
}
exports.playLeyLineConvergence = function(game, power) {
  var resources = []
  for (var i = 0; i < game.cells.length; i++) { 
    var cell = game.cells[i]
    if (cell.tileType() === 'magic') resources.push(cell._id)
  }
  var turn = new Turn()

  matcher.matchMagic(game, turn, resources)
  matcher.clearMatches(game, resources)
  board.reseat(game, turn)

  return turn.serialize()
}
exports.playLegerdemain = function(game, power, tiles) {
  if (!(tiles && tiles.length === 2)) return false

  var investigator = game.investigator
  game.swapCells(tiles[0], tiles[1])
  game.investigator = investigator

  var results = {swap: tiles}
  return results
}
exports.playTheGreatWork = function(game, power, tiles) {
  if (!(tiles && tiles.length === 1)) return false

  var index = tiles.pop()
  var cell = game.cells[index]

  switch (cell.tileId) {
    case 'tincture':
      cell.tileId = 'laudanum'
      break
    case 'rite':
      cell.tileId = 'grimoire'
      break
    case 'bandages':
      cell.tileId = 'morphine'
      break
    case 'funds':
      cell.tileId = 'artifact'
      break
    case 'firearm':
      cell.tileId = 'dynamite'
      break
  }
  var turn = new Turn()
  turn.transform(index, cell.serialize())

  return turn.serialize()
}
exports.playBodyWarpingOfGorgoroth = function(game, power, tiles) {
  if (!(tiles && tiles.length === 1)) return false

  var index = tiles.pop()
  var cell = game.cells[index]
  var monsterId = cell.monster._id

  var monster = dealer.newMonster(game, monsterId)
  cell.monster = monster

  var turn = new Turn()
  turn.transform(index, {monster: cell.monster.serialize()})
  return turn.serialize()
}
exports.playSolomonsWisdom = function(game, power, tiles) {
  if (!(tiles && tiles.length === 1)) return false

  var index = tiles[0]
  var cell = game.cells[index]
  var y = cell.y()
  var x = cell.x()
  var row = game.cells.filter(function(c){ return c.y() === y })
  for (var i =0; i < row.length; i++) {
    var fromCell = row[i]
    var oppositeIndex = game.indexFromCoordinates(x, i)

    if (fromCell._id === oppositeIndex) continue

    game.swapCells(fromCell._id, oppositeIndex)
  }
  return {}
}
exports.playSupernalBinding = function(game, power, tiles) {
  if (!(tiles && tiles.length === 1)) return false

  var index = tiles[0]
  game.addEffect('binding', index)
  return true
}
exports.playChantOfThoth = function(game, power) {
  var index = tiles[0]
  game.addEffect('insight', true)
  return true
}
exports.playCallTheBeast = function(game, power, tiles) {
  if (!(tiles && tiles.length === 1)) return false

  var index = tiles[0]
  var cell = game.cells[index]
  if (cell.y() === 0) return false

  var monster = cell.monster

  game.cells[index].monster = null
  game.cells[index].tileId = null
  board.reseat(game)

  game.cells[cell.x()].monster = monster
  game.cells[cell.x()].plot = null
  game.cells[cell.x()].tileId = 'monster'

  return true
}

exports.play = function(game, powerId, tiles) {
  var power = null
  for (var i = 0; i < game.investigator.powers.length; i++) {
    if (game.investigator.powers[i]._id === powerId) {
      power = game.investigator.powers[i]
      break
    }
  }

  if (!power) return false

  var cost = power.magic
  if (cost > game.investigator.magic.current) return false

  var results = {}
  switch (powerId) {
    case 'as_above_so_below':
      results = exports.playAsAboveSoBelow(game, power)
      break
    case 'body_warping_of_gorgoroth':
      results = exports.playBodyWarpingOfGorgoroth(game, power, tiles)
      break
  	case 'legerdemain':
  	  results = exports.playLegerdemain(game, power, tiles)
      break
    case 'ley_line_convergence':
      results = exports.playLeyLineConvergence(game, power)
      break
    case 'faustian_riches':
      results = exports.playFaustianRiches(game, power)
      break
    case 'flesh_ward':
      results = exports.playFleshWard(game, power)
      break
    case 'supernal_binding':
      results = exports.playSupernalBinding(game, power, tiles)
      break
    case 'solomons_wisdom':
      results = exports.playSolomonsWisdom(game, power, tiles)
      break
    case 'the_great_work':
      results = exports.playTheGreatWork(game, power, tiles)
      break
    case 'chant_of_thoth':
      results = exports.playChantOfThoth(game, power)
      break
    case 'call_the_beast':
      results = exports.playCallTheBeast(game, power, tiles)
      break
  }

  game.stats.powers++

  // investigator.magic.current = Math.max(investigator.magic.current - power.magic, 0)
  results.cost = cost

  return results
}
