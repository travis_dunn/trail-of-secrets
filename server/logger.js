var winston = require('winston');
var container = new winston.Container();
var MongoDB = require('winston-mongodb').MongoDB;
var CONFIG = require('config');

function clone (src) {
  return JSON.parse(JSON.stringify(src));
}

var errorConfig = clone(CONFIG.Database.Logs)
errorConfig.collection = 'errors'
var profileConfig = clone(CONFIG.Database.Logs)
profileConfig.collection = 'profiles'
var analyticsConfig = clone(CONFIG.Database.Logs)
analyticsConfig.collection = 'analytics'

container.add('app', {
  transports: [new (MongoDB)(CONFIG.Database.Logs)]  
});
container.add('error', {
  transports: [new (MongoDB)(errorConfig)]  
});
container.add('profile', {
  transports: [new (MongoDB)(profileConfig)]
});
container.add('analytics', {
  transports: [new (MongoDB)(analyticsConfig)]  
});

// log errors to database in production
if (process.env.NODE_ENV === 'production') {
  process.on('uncaughtException', function(err) {
    console.log(err.message);
    console.log(err.stack);
    console.trace()
    
    exports.error(err.message, {stack: err.stack}, function() {
      process.exit(1);
    })
  });
}

exports.profile = function profile(id, message, metaData) {
  container.get('profile').profile(id, message, metaData)
}
exports.error = function error(message, metaData, cb) {
  container.get('error').error(message, metaData)
}
exports.log = function log(logger, message, metaData) {
  container.get(logger).info(message, metaData)
}