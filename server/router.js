var facebook = require('./mechanics/facebook')

exports.init = function init(ss) {
    // main client has news, help, about, contact info, etc.
    ss.client.define('main', {
      view: 'app.html',
      css:  ['libs/bootstrap.cdn', 'libs/bootstrap-responsive.css', 'app.css'],
      code: ['libs/jquery.min.js', 'libs/bootstrap.js', 'app'],
      tmpl: '*'
    });
    ss.http.route('/', function(req, res){
      res.serveClient('main');
    });

    // all gameplay happens in the game client
    ss.client.define('game', {
      view: 'game.html',
      css:  ['libs/bootstrap.cdn', 'libs/bootstrap-responsive.css', 'libs/animate.css', 'app.css', 'game.cdn', 'tiles.cdn', 'media/tablet.css', 'media/smartphone.css'],
      code: ['libs/jquery.min.js', 'libs/bootstrap.js', 'libs/modal-responsive-fix.min.js', 'libs/touchit.js', 'libs/jquery.cookie.js', 'libs/hide-address-bar.js', 'libs/soundmanager2.min.js', 'app', 'libs/facebook.js', 'game'],
      tmpl: '*'
    });
    ss.http.route('/game', function(req, res){
      res.serveClient('game');
    });

    // leaderboards and tournaments and friends finders
    ss.client.define('leaderboards', {
      view: 'leaderboards.html',
      css:  ['libs/bootstrap.cdn', 'libs/bootstrap-responsive.css', 'app.css'],
      code: ['libs/jquery.min.js', 'libs/bootstrap.js', 'app/entry.js', 'app/leaderboards.js', 'app/player.js', 'libs/facebook.js'],
      tmpl: '*'
    });
    ss.http.route('/leaderboards', function(req, res){
      res.serveClient('leaderboards');
    });

    // simple help page
    ss.client.define('help', {
      view: 'help.html',
      css:  ['libs/bootstrap.cdn', 'libs/bootstrap-responsive.css', 'app.css'],
      code: ['libs/jquery.min.js', 'libs/bootstrap.js', 'app/entry.js'],
      tmpl: '*'
    });
    ss.http.route('/help', function(req, res){
      res.serveClient('help');
    });

    // Facebook channel route for CORS handling
    ss.client.define('channel', {
      view: 'channel.html',
      css:  [],
      code: [],
      tmpl: '*'
    });
    ss.http.route('/channel', function(req, res){
      res.serveClient('channel');
    });    

    // server-side payment process callback handler (e.g. Facebook payments, GREE, &c)
    ss.http.route('/payments', function(req, res) {
      facebook.paymentCallback(req, function(err, result) {
        var str = JSON.stringify(result) || ''

        res.writeHead(200, {
          'Content-Length': Buffer.byteLength(str),
          'Content-Type': 'text/html'
        });
        return res.end(str);
      })
    });
}