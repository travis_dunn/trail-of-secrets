/* 
Interpolate CDN host (e.g. S3) asset paths in css wrapper for SocketStream 0.3

# Usage

1. Initialize the formatter your app.js file, passing in a config with `{host: '...'}`, e.g.

    ss.client.formatters.add(require('./cdn-wrapper'), {host: 'https://s3.amazonaws.com/my-bucket'})

2. Save your css files with image urls using the .cdn extension - `client/css/my-css-file.cdn` - and then declare images 
in your css in the following form to interoplate them with the host

    background: url(/images/bg/my-image.png)

This would process to:

    background: url(https://s3.amazonaws.com/my-bucket/images/bg/my-image.png)

Note: at the moment only images are supported. The following regex matches the css:

    /\((.*?(png|gif|jpg|jpeg))\)/g

*/ 

var fs = require('fs')

exports.init = function(root, config) {
  var host = config.host

  return {
    name: 'SSCDN',
    extensions: ['cdn'],
    assetType: 'css',
    contentType: 'text/css',
    compile: function(path, options, cb) {
      var input = fs.readFileSync(path, 'utf8');
      if (host && host.length) {
        input = input.replace(/\((.*?(png|gif|jpg|jpeg))\)/g, function(match, p1, p2) {
          return '('+host+p1+')'
        })
      }
      cb(input)
    }
  };
};