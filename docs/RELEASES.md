# Release Schedule

+ Smoke Test: Cultes de Ghouls, BBGameZone
+ Gameplay Balance Test: BoardGameGeek, IndieDB
+ Leaderboard QA: Various topsites HTML5 portals
+ Tech Demo: Node.js list, SocketStream dev
+ UI Test: TIGSource
+ Artist Preview: DeviantArt, Pixelation, PixelJoint
+ Facebook accounts: Facebook app directory
+ GREE accounts: Gree platform
+ Mobage accounts: Mobage platform
+ Mobile QA: TouchArcade
+ Alpha Playtest: PennyArcade Indie Board, FB Groups
+ Beta Playtest: QuarterToThree, RPGNet